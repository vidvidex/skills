# Mail server

- https://www.binarytides.com/install-postfix-dovecot-debian/


## Prepare

- naloži `mailutils`
  ```sh
  apt install mailutils
  ```

- naredi certifikat s SAN za `mail.example.com`, `servername.example.com`, `IP_SERVERJA` *(glej [CA.md](CA.md))* in ga daj v `/etc/ssl/private` oz `/etc/ssl/certs`
  

## DNS

- v `/etc/bind/db.example.net` dodaj:
  ```txt
  mail  IN  A 10.0.10.1
  example.net.  IN  MX  10  mail.example.net.
  ```
  (mail server za domeno `example.net` je na `10.0.10.1` oz `mail.example.net`)


## Postfix

- install
  ```sh
  apt install postfix
  ```
  - izberi "Internet Site"
  - vpiši domain name (npr. `globex-isp.com`)

- v `/etc/postfix/main.cf`:
  - iz `mydestination` izbriši vse razen `localhost`
    - ostale domene so konfigurirane z `virtual_mailbox_domains` (a je to res treba? mogoče lahko direktno. Nisem preveril)
    - https://www.howtoforge.com/community/threads/dovecot-mail-delivery-system-returns-unknown-user-and-client-authentication-fails.60511/#post-291837

  - dodaj:
    ```txt
    virtual_transport = lmtp:unix:private/dovecot-lmtp

    smtpd_sasl_type = dovecot
    smtpd_sasl_path = private/auth
    smtpd_sasl_auth_enable = yes

    smtpd_tls_auth_only = yes

    virtual_mailbox_domains = /etc/postfix/vdomains # Tu so virtualne domene za postfix namesto v mydestination
    ```
  - popravi path do ssl certifikatov

  - če IPv6 dela težave ga lahko izklopiš tako, da `inet protocols` nastaviš na `ipv4` namesto `all`
    ```txt
    inet protocols = ipv4
    ```
    

- Dodaj domene, za katere je zadolžen ta server:
  - v `/etc/postfix/vdomains` dodaj:
    ```txt
    globex-isp.com  OK
    ```
  - `postmap /etc/postfix/vdomains`
    
- v `/etc/postfix/master.md` odkomentiraj vstico
  ```txt
  submission inet n       -       -       -       -       smtpd
  ```

- restart postfix
  ```sh
  systemctl restart postfix
  ALI
  postfix reload
  ```

## Dovecot

- install
  ```sh
  apt install dovecot-core dovecot-imapd dovecot-lmtpd
  ```

- naredi direktorij za maile in uporabnika, ki jih bo managal
  ```sh
  mkdir /var/vmail

  groupadd -g 5000 vmail
  useradd -r -g vmail -u 5000 vmail -d /var/vmail

  chown -R vmail:vmail /var/vmail/
  ```
  
- v `/etc/dovecot/conf.d/10-mail.conf` spremeni `mail_location` na:
  ```txt
  mail_location = maildir:/var/vmail/%n # Mail za vsakega uporabnika je v svoji mapi v /var/vmail
  ```

- v `/etc/dovecot/conf.d/10-master.conf`:
  - vklopi imaps
    ```txt
    service imap-login {
      inet_listener imap {
        #port = 143
      }
      inet_listener imaps {
        port = 993
        ssl = yes
      }
    }
    ```
  - naredi socket za lmtp (za lažjo dopolnitev lahko kopiraš path od SASL socketa in samo spremeniš zadnji del na `dovecot-lmtp`)
    ```txt
    service lmtp { # Glej da bo to znotraj tega sectiona
      unix_listener /var/spool/postfix/private/dovecot-lmtp {
        mode = 0600
        user = postfix
        group = postfix
      }
    }
    ```
  - naredi SASL socket (pod `service auth` je večina že, manjka `user` in `group`)
    ```txt
    unix_listener /var/spool/postfix/private/auth {
      mode = 0666
      user = postfix
      group = postfix
    }
    ```
- v `/etc/dovecot/conf.d/10-ssl.conf` popravi pot do certifikatov in spremeni ssl na `ssl = required`

- restart
  ```sh
  systemctl restart dovecot
  systemctl status dovecot
  ```

- v `/etc/dovecot/conf.d/10-auth.conf`:
  - odkomentiraj `disable_plaintext_auth = yes`
  - odkomentiraj in spremeni `auth_username_format = %n` da bo iskalo samo username brez domene (https://serverfault.com/questions/646413/how-to-correct-dovecot-user-login-to-use-full-domain-email-in-this-format-myuse)
  - k `auth_mechanisms` dopiši `login`
    ```txt
    auth_mechanisms = plain login
    ```
  - na koncu fila izberi željeni način avtentikacije:
    ```txt
    #!include auth-system.conf.ext        # Sistemski uporabniki
    #!include auth-passwdfile.conf.ext    # Virtualni uporabniki
    #!include auth-ldap.conf.ext          # LDAP uporabniki
    ```
  - konfiguriraj željen način avtentikacije

- za debugging avtentikacije (in drugih stvari) se v `/etc/dovecot/conf.d/10-logging.conf` da vklopiti verbose logging in podobne nastavitve.
  - logi so v `/var/log/mail.neki` in `/var/log/syslog`

### Avtentikacija s sistemskimi uporabniki

- naredi uporabnike, npr.
  ```sh
  adduser franci
  ```

- v `/etc/dovecot/conf.d/auth-system.conf.ext` popravi na :
  ```txt
  passdb {
    driver = pam
  }

  userdb {
    driver = static
    args = uid=vmail gid=vmail home=/var/vmail/%n   # Namesto vmail lahko tudi 5000 (id userja in skupine)
  }
  ```

- sistemski uporabniki morajo imeti write permission za mail directory (`/var/vmail`)
  ```sh
  chmod 777 -R /var/vmail

  ali

  usermod -aG franci vmail
  chmod 760 -R /var/vmail
  ```


### Avtentikacija z virtualnimi uporabniki

- v `/etc/dovecot/conf.d/auth-passwdfile.conf.ext` popravi na :
  ```txt
  passdb {
    driver = passwd-file
    args = scheme=CRYPT username_format=%u /etc/dovecot/users
  }

  userdb {
    driver = static
    args = uid=vmail gid=vmail home=/var/vmail/%n   # Namesto vmail lahko tudi 5000 (id userja in skupine)
  }
  ```

- dodaj uporabnike za `dovecot` (so v `/etc/dovecot/users`):

  - ```sh
    doveadm pw -p Passw0rd! >> /etc/dovecot/users
    doveadm pw -p Passw0rd! >> /etc/dovecot/users # 2x ker rabimo za 2 uporabnika in sta gesli zaradi salta različni
    ```
  - v `/etc/dovecot/users` dopiši username, da zgleda tako: (**NOTE: to je bilo napisano preden sem pri ldap auth ugotovil da potrebujem `auth_username_format = %n`, mogoče bo zaradi tega tu le username, brez domene**)
    ```txt
    hans@globex-isp.com:{CRYPT}$1$JdyRMcO6$qUwKZT40EVp/oIpVfAEXF
    anna@globex-isp.com:{CRYPT}$1$fdsjfdskjfkFBfkdbfdskbkhBfkdsb
    ```

### Avtentikacija z LDAP

- Naloži `dovecot-ldap`
  ```sh
  apt install dovecot-ldap
  ```

https://help.ubuntu.com/community/DovecotLDAP

- v `/etc/dovecot/conf.d/auth-ldap.conf.ext` popravi na :
  ```txt
  passdb {
    driver = ldap
    args = /etc/dovecot/dovecot-ldap.conf.ext4
  }

  userdb {
    driver = static
    args = uid=vmail gid=vmail home=/var/vmail/%n   # Namesto vmail lahko tudi 5000 (id userja in skupine)
  }
  ```

- v `/etc/dovecot/dovecot-ldap.conf.ext` popravi (vse možnosti so že napisane):
  ```txt
  hosts = intsrv.skills39.local
  dn = cn=admin,dc=skills39,dc=local
  dnpass = Passw0rd!
  base = dc=skills39,dc=local
  user_attrs = homeDirectory=home
  user_filter = (&(objectClass=posixAccount)(uid=%n))
  ```

## Thunderbird

- install
  ```sh
  apt install thunderbird
  ```

## Pošiljanje groupu

- v `/etc/postfix/main.cf` dodaj
  ```txt
  virtual_alias_maps = hash:/etc/postfix/virtual
  ```

- v `/etc/postfix/virtual` napiši aliase: (kar pošlješ na staff je forwardano fritzu in inge)
  ```txt
  staff@skills39.com fritz@skills39.com, inge@skills39.com
  ```

- poženi
  ```sh
  postmap /etc/postfix/virtual
  ```

