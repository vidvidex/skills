# SSH

## Dovoli prijavo za root

- v `/etc/ssh/sshd_config` spremeni `PermitRootLogin`
    ```txt
    PermitRootLogin yes
    ```
- restart
    ```sh
    systemctl restart sshd
    ```

### Testiranje

- poskusi se prijaviti kot `root`
    ```sh
    ssh root@10.0.10.1
    ```
- če je `PermitRootLogin` izklopljen bo izpisalo da je geslo napačno


## Dostop samo za določene hoste:

- v `/etc/ssh/sshd_config` **na dno** (`Match` velja do naslednjega `Match`a oziroma do konca fila) dodaj
    ```txt
    PermitRootLogin no

    Match Address 100.43.60.68
        PermitRootLogin yes

    Match Address 2001:db8::b
        PermitRootLogin yes
    ```
- restart
    ```sh
    systemctl restart sshd
    ```

### Testiranje

- preveri če se lahko prijaviš samo iz določenih hostov
    ```sh
    ssh 10.0.10.1
    ```


## Key-based authentication

- https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server

- na računalniku iz katerega se želimo povezati na nek server poženeš
    ```sh
    ssh-keygen
    ```
    in specificiraš nek custom path če je potrebno, v večino primerov pa lahko pustiš default. Gesla ni potrebno nastavljati

- nekako spravi .pub key na remote server:
    - s `ssh-copy-id USERNAME@REMOTE_SERVER_ADDRESS` (če prej nisi pustil default ime moraš specificirati še ime (`-i`))
    - s `cat` na konzolo izpišeš vsebino .pub in nato kopiraš v `/home/USERNAME/.ssh/authorized_keys` na remote serverju

- če je potrebno lahko v `/etc/ssh/sshd_config` na remote serverju izklopiš password login:
    ```txt
    PasswordAuthentication no
    ```

### Testiranje

- preveri če se lahko prijaviš in te ob prijavi ne vpraša za geslo
    ```sh
    ssh 10.0.10.1 -i .ssh/kljuc.key
    ```
