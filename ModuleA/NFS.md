# NFS

https://cloud.netapp.com/blog/azure-anf-blg-linux-nfs-server-how-to-set-up-server-and-client

https://www.thegeekdiary.com/understanding-the-etc-exports-file/

https://www.howtoforge.com/tutorial/install-nfs-server-and-client-on-debian

## Server

- Naloži
    ```sh
    apt install nfs-kernel-server
    ```

- v `/etc/exports` dodaj
    ```txt
    /data/www   10.0.10.0/24(rw,sync)
    ```

- restart
    ```sh
    systemctl restart nfs-kernel-server
    ```


## Client


- Naloži
    ```sh
    apt install nfs-common
    ```
- V `/etc/fstab` dodaj:
    ```txt
    10.0.20.1:/data/www     /var/www/html   nfs     defaults    0   0
    ```
    ```sh
    mount -a
    ```
    
## Testranje

- na clientu mountaš NFS share, če je pravilno mountan (noter vidiš sharane file) potem dela
  ```sh
  mount -a
  ```

- preveriš še, če se pravilno mounta ob rebootu
  ```sh
  reboot
  ```