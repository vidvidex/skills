# rsync

## Server

- install
  ```sh
  apt install rsync
  ```


- kopiraj sample config file

  ```sh
  cp /usr/share/doc/rsync/examples/rsyncd.conf /etc/
  ```

- v `/etc/confd.conf` spremeni `path` in `read only` na `no` če je potrebno spremeni ime modula (v example filu je default `ftp`)

- restart
  ```sh
  systemctl restart rsync
  ```

## Client

- ukaz za prenos vsebine `/data` na remote server:
  ```sh
  rsync -a /data/ rsync://ispsrv.globex-isp.com:/backup # tu je backup ime modula na rsync serverju. Protokol rsync:// je potreben da uprabi rsync daemon namesto ssh
  ```

- cronjob:
  ```sh
  crontab -e
  
  * * * * * /bin/bash /usr/local/bin/backup.sh
  ```

- mogoče je potrebno spremeniti permissione za `/data/`

## Testiranje

- na clientu v `/data/` daš nek file in počakaš minuto, da se cronjob izvede. Nato pogledaš če se je file kopiral na server