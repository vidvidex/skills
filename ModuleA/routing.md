# Routing


## Forwardiranje

- za vklop routinga na linuxu je v `/etc/sysctl.conf` potrebno nastaviti (obe opciji že obstajata, a sta nastavljeni na 0)
    ```txt
    net.ipv4.ip_forward=1
    net.ipv6.all.ip_forwarding=1
    ```
- in nato restartati
    ```sh
    reboot
    ```

## Poor man's static route

- nastavljanje statičnih rout, ki se ne izbrišejo po restartu je komplicirano (ali pa se samo meni nikoli ni dalo ugotoviti kako se to pravilno naredi)
- lažja in hitrejša rešitev je uporaba default gatewaya v `/etc/network/interfaces`. S tem lahko ves promet, za katerega naprava ne ve kam je namenjen pošlješ na nek naslov