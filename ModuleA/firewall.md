# NFTables firewall

- install
    ```sh
    apt install nftables
    ```

- načeloma se konfigurira z `nft` ukazom a je lažje pisati pravila direktno v conf file

- konfiguracija je v `/etc/nftables.conf`

- primer konfiguracije:
    ```txt
    #!/usr/sbin/nft -f

    flush ruleset

    define HQ_CLIENTS = {
        10.1.10.0/24,
        10.8.0.0/24
    }

    define PRIVATE_CLOUD = {
        10.0.10.0/24,
        10.0.20.0/24
    }

    define INTERNAL = {
        $HQ_CLIENTS,
        $PRIVATE_CLOUD
    }

    table inet firewall {
        chain input {
            type filter hook input priority 0; policy drop;

            # Permit established and related, drop invalid
            ct state established,related accept;

            # Permit SSH for easier testing
            tcp dport ssh accept;

            # Permit local networks
            ip saddr 10.0.0.0/8 accept;

            # Permit ICMP
            icmp type echo-request accept;
            icmpv6 type echo-request accept;
        }
        chain forward {
            type filter hook forward priority 0; policy drop;

            # Permit established and related, drop invalid
            ct state established,related accept;

            # Permit traffic from HQ clients to Private cloud
            ip saddr $HQ_CLIENTS ip daddr $PRIVATE_CLOUD accept;

            # Permit DNS, HTTP, HTTPS, SMTP, IMAP, FTP to DMZ servers
            ip daddr 10.0.10.0/24 udp dport 53 accept;
            ip daddr 10.0.10.0/24 tcp dport { http, https, 25, 994, 587, 143, 21, 22} accept;

            # Permit internal traffic to internal servers
            ip saddr $INTERNAL ip daddr 10.0.20.0/24 accept;
        }
        chain output {
            type filter hook output priority 0;
        }
    }

    table ip nat {
        chain prerouting {
            type nat hook prerouting priority -100;
        }
        chain postrouting {
            type nat hook postrouting priority 100;
            ip daddr 10.0.0.0/8 accept; # Ne NATaj prometa, ki bo šel čez VPN
            oifname "ens192" masquerade;
        }
    }
    ```

- poglej katere porte je potrebno odpreti:
    ```sh
    netstat -tnlp
    ```
- default policy je `policy accept;`

## Testiranje

### HTTP in HTTPS

```sh
curl http://www.skills39.com
curl https://www.skills39.com -k # -k ignorira napako zaradi nezaupanja ssl certifikatu
```

### DNS

```sh
systemctl restart bind9 # Najprej je potrebno restartat bind da počisti cache
dig skills39.com
```

### SSH

```sh
 ssh root@10.0.10.1
```
### NAT

- iz hosta za NATom narediš http request na nek webserver

- v `/var/log/apache2/access.log` pogledaš iz katerega IPja je prišel request

### IPSec

- pogledaš če se site2site tunel pravilno postavi (glej [site2site-VPN.md](site2site-VPN.md))
