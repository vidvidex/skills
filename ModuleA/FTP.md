# FTP

- https://www.vultr.com/docs/setup-pure-ftpd-with-tls-on-debian-9

- https://download.pureftpd.org/pub/pure-ftpd/doc/README.Virtual-Users

## Pure-FTPd

- install
    ```sh
    apt install pure-ftpd
    ```

- dodaj virtualnega uporabnika, ki je vezan na `www-data`
    ```sh
    pure-pw useradd webmaster -u www-data -d /var/www/html # -d ga chroota v tist directory
    pure-pw mkdb # Naredi db
    ```

- enable PureDB (virtual users)
    ```sh
    ln -s /etc/pure-ftpd/conf/PureDB /etc/pure-ftpd/auth/50pure
    ```

- ker je `webmaster` vezan na `www-data`, ki ima nizek uid (33), je potrebno spremeniti min uid, ki ga pure-ftpd še spusti čez
    ```sh
    echo "10" > /etc/pure-ftpd/conf/MinUID
    ```

- vklopi ssl (2 = force TLS)
    ```sh
    echo "2" > /etc/pure-ftpd/conf/TLS
    ```

- naredi certifikat za `dmzsrv2.skills39.local` na `intsrv`, združi crt in key v `/etc/ssl/private/pure-ftpd.pem` na `dmzsrv2` (najprej key potem crt)


- restart
    ```sh
    systemctl restart pure-ftpd
    ```

## Testiranje

- na clientu z GUIjem se z FileZillo povežeš na server in poskusiš tranferat kak file
- na serverju preveriš da je lastnik uploadane datoteke res `www-data`
    ```sh
    ls -la
    ```
- če nimaš GUIja lahko uporabiš `ftp` ali `lftp` ukaza
