# High Availability

Opcije:
- `haproxy` za load balancing, `ucarp` ali `keepalived` poskrbita za floating IP med serverjema
- samo `ucarp` ali `keepalived` za floating IP, ostanemo brez load balancinga a je konfiguracije manj

2 Guida za keepalived + haproxy:
- https://raymii.org/s/articles/Adding_IPv6_to_a_keepalived_and_haproxy_cluster.html

- https://e-mc2.net/keepalived-documentation-nightmare


## keepalived

- https://stackoverflow.com/questions/35482083/how-to-create-floating-ip-and-use-it-to-configure-haproxy

- https://github.com/acassen/keepalived/blob/dc049d864902c74121f6822ba5c6db4c6f64f25b/doc/samples/keepalived.conf.vrrp.localcheck

- install
    ```sh
    apt install keepalived
    ```

- kopiraj default config
    ```sh
    cp /usr/share/doc/keepalived/keepalived/samples/keepalived.conf.sample /etc/keepalived/keepalived.conf
    ```
- iz configa izbriši vse razen:
    ```txt
    vrrp_instance VI_1 {
        interface ens192
        virtual_router_id 50
        priority 100    # Na master 100, na slave 101
        advert_int 1
        virtual_ipaddress {
            10.0.10.100
        }
        virtual_ipaddress_excluded { # IPv4 in IPv6 ne smeta biti skupaj https://e-mc2.net/keepalived-documentation-nightmare
            2001:db8:a:a::100
        }
    }
    ```
- restart
    ```sh
    systemctl restart keepalived
    ```

### Testiranje

- pogledaš če ima enden izmed serverjev (master) še dodaten IPv4 in IPv6 naslov (`10.0.10.100` in `2001:db8:a:a::100`)
    ```sh
    ip a
    ```
- server, ki ima trenutno floating IP ugasneš in pogledaš, če bo floating IP prešel na drugi server
    ```sh
    shutdown now
    reboot
    ```




## ucarp

**nima podpore za IPv6, glej keepalived**

sample config za interface je v `/usr/share/doc/ucarp/README.Debian`

- `apt install ucarp`

- na obeh serverjih spremeni `/etc/network/interfaces`:

  ```conf
  auto ens192
  iface enp0s3 inet static
      address 10.10.10.79
      netmask 255.255.255.0
      gateway 10.10.10.1
      dns-nameservers 8.8.8.8
      # Dodaj od tu naprej
      ucarp-vid      1
      ucarp-vip      VIRTUAL_IP
      ucarp-password PASSWORD
      ucarp-advskew 100 # Advertisement skew [1-255], nižja številka ima prioriteto
      ucarp-advbase 1 # Vsake koliko sekund oglašuje
      ucarp-master yes|no # master naj ima nižji skew (npr 1) ostali pa npr 100

  iface ens192:ucarp inet static
      address VIRTUAL_IP
      netmask 255.255.255.0
  ```

- `systemctl restart networking` (če ne dela, restart server)

- `systemctl status networking` napiše če je MASTER ali BACKUP


## haproxy

sample config je v `/usr/share/doc/haproxy/arhitecture.txt`

- `apt install haproxy`

- v `/etc/haproxy/haproxy.cfg` dodaj:

  ```conf
  listen NAME
  bind FLOATING_IP:80
  mode http
  balance roundrobin
  server s1 10.0.10.1:80 check # pravi IP serverja
  server s2 10.0.10.1:80 check # pravi IP serverja
  ```

- v `/etc/apache2/ports.conf` spremeni `Listen 80` v `Listen 10.0.10.1:80` (le pravi ip serverja)

- `systemctl restart apache2`

- `systemctl restart haproxy`

- v `/usr/share/ucarp/vip-up` dodaj `systemctl start haproxy`, da se `haproxy` prižge ko se prižge floating interface. Lokacija `vip-up` je napisana tudi v `/usr/share/doc/ucarp/README.Debian`
