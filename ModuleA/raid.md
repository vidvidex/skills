# Raid 6

- `apt install mdadm`

- poglej diske

  ```sh
  ldblk
  ```

- naredi RAID 6

  ```sh
  mdadm --create /dev/md0 --level=6 --raid-devices=4 /dev/sdb /dev/sdc /dev/sdd /dev/sde --verbose
  ```

- formatiraj

  ```sh
  mkfs.ext4 /dev/md0
  ```

- naredi mount point

  ```sh
  mkdir /data
  ```

- v `/etc/fstab` dodaj

  ```conf
  /dev/md0 /data  ext4 defaults 0 0
  ```

- mount (mogoče treba še spremeniti ownerja)

  ```sh
  mount -a
  ```

- da se raid pravilno postavi po rebootu:
  ```sh
  mdadm --detail --scan | tee -a /etc/mdadm/mdadm.conf
  update-initramfs -u
  ```

- `man mdadm` ima example

## Testiranje

- z `lsblk` pogledaš če je čez vse 4 diske postavljena 2 GB velika particija
  ```sh
  lsblk
  ```

- pogledaš če se RAID pravilno postavi ob rebootu
  ```sh
  reboot
  ```