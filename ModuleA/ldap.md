# LDAP

## OpenLDAP

https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-openldap-and-phpldapadmin-on-an-ubuntu-14-04-server

- install
    ```sh
    apt install slapd ldap-utils
    dpkg-reconfigure slapd # Vpraša za več nastavitev
    ```


## JXplorer

- naloži in poženi (če iz terminala včasih izpiše kakšno uporabno napako, npr napačna polja)
    ```sh
    apt install jxplorer
    jxplorer
    ```
- prijava: 
    - Host: ldap server (10.0.20.1)
    - Level: User + Password
    - User DN: cn=admin,dc=skills39,dc=local
    - Password: Passw0rd!

- nov OU:
    1. v levem drevesu desen klik na skills39
    2. new
    3. pod RDN: `ou=IME_OU`
    4. v Selected Classes le `organizationalUnit`
    5. ničesar ni potrebno dodajat, le klik na submit

- nov user:
    1. v levem drevesu desen klik na nek OU
    2. new
    3. pod RDN: `cn=IME_UPORABNIKA`
    4. v SelectedClasses: `inetOrgPerson`, `posixAccount`, `person`
    5. dodaš:
        - `homeDirectory`
        - `sn`, (surname), lahko kar ime uporabnika
        - `uid`, po tem ldap išče uporabnike, enako kot username/cn (npr. fritz)
        - `uidNumber`, npr 2001, 2002, ...
        - `gidNumber`, POSIX group id, npr 201, 202, ...
        - `mail`
        - `userPassword`

- nov group:
    - v levem drevesu desen klik na parenta od te skupine
    - new
    - pod RND: `cn=IME_SKUPINE`
    - v SelectedClasses: `groupOfNames`
    - dodaš vsaj enega memberja v obliki `cn=inge,ou=sales,dc=skills39,dc=local`

- ko imaš enega (OU, uporabnika ali skupino) lahko kloniraš (manj dela)

## Linux login

https://www.techrepublic.com/article/how-to-authenticate-a-linux-client-with-ldap-server/

- install
    ```sh
    apt install libnss-ldapd libpam-ldapd ldap-utils
    ```
    *obstajata tudi `libnss-ldap` in `libpam-ldap`, ki sta napisana v večini guidov, a se z njima client noče povezati na LDAP server*

- v config wizardu napiši:
    - IP LDAP serverja
    - search base (ponavadi `dc=skills39,dc=local`, lahko pa tudi `ou=sales,dc=skills39,dc=local` za filtriranje glede na ou)
    - pri "Name services to configure" izberi `passwd`, `group` in `shadow`

- v `/etc/pam.d/common-session` na dno dodaj: 
    ```txt
    session optional pam_mkhomedir.so skel=/etc/skel umask=077
    ```
    *(naredi home directoryje, brez teh lightdm? ne dovoli logina)*


### Offline login

- https://serverfault.com/a/1004660

- naloži package
    ```sh
    apt install libnss-db nss-updatedb libpam-ccreds
    ```
- poženi `pam-auth-update` in preveri da je vse označene (TODO: nekoč ugotovi katere opcije so potrebne)

- v `/etc/nsswitch.conf` pri `passwd` in `group` dodaj `[NOTFOUND=return] db`

- updataj lokalni db in preveri če dela
    ```sh
    nss_updatedb ldap
    cc_dump
    cc_test -validate any fritz Passw0rd!
    su -l fritz # Te prijavi kot fritz
    ```
- mogoče je potreben še restart


### Debugging

- za iskanje uporabnikov v LDAPu se uporablja `uid`, glej da ga nastaviš na uporabnikov username (npr. fritz). Če je to nastavljeno narobe bo pri poskusu logina s tem uporabnikom v `/var/log/auth.log` napisalo Unknown user

- mogoče je potreben restart preden login pravilno dela
- če je homedir na NFS serverju poskrbi, da ima user read in write permisison, npr. na NFS serverju:
    ```sh
    chmod 777 /data/home
    ```

### Testiranje

- prijaviš se z ldap uporabniškim imenom in geslom
- če prijava ne uspe se prijaviš z `root` in v `/var/log/auth.log` pogledaš kakšna je napaka
    ```sh
    cat /var/log/auth.log
    ```
