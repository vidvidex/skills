# Syslog

- https://www.tecmint.com/install-rsyslog-centralized-logging-in-centos-ubuntu/

- https://www.howtoforge.com/how-to-setup-rsyslog-server-on-ubuntu-1804/

- https://codepre.com/how-to-set-up-a-debian-linux-syslog-server.html

- https://unix.stackexchange.com/questions/477403/rsyslog-server-template-consideration-for-multiple-remote-hosts

`man rsyslog.conf`

## Server

- v `/etc/rsyslog.conf` odkomentiraj remote UDP povezavo
    ```txt
    module(load="imudp")
    input(type="imtpc" port="514") 
    ```

- na vrh "RULES" dodaj filter za dmz serverje:
    ```sh
    if $hostname startswith "dmzsrv1" then {
        local3.*    -/log/dmz1-web.log
        & stop
    }

    if $hostname startswith "dmzsrv2" then {
        ftp.*    -/log/dmz2-ftp.log
        & stop
    }

    if $hostname startswith "dmzsrv" then -/log/dmz-dump.log
    & stop
    ```
- `systemctl restart rsyslog`

## Client

### Global syslog

- v `/etc/rsyslog.conf` na vrh "RULES" dodaj (pošlje vse na 10.0.10.254 prek UDP)
    ```txt
    *.*     @10.0.10.254
    ```
- `systemctl restart rsyslog`

### Apache

- v config filu od spletne strani (npr. `000-default.conf`) spremeni `CustomLog` na:
    ```txt
    CustomLog "| /usr/bin/logger -p local3.info" combined
    ```
    `local3` mora biti enako kot pri serverju
- `systemctl restart apache2`


## Testiranje

- na clientih (`dmzsrv1` in `dmzsrv2`) narediš nekaj prometa za log (npr. restaraš kak service, greš na website, ...)
    ```sh
    systemctl restart apache2
    curl www.example.net
    ```
- na serverju (`cloudfw`) pogledaš če se logi zapisujejo v pravi file
    - web access iz `dmzsrv1` v `/log/dmz1-web.log`
    - ftp iz `dmzsrv2` v `/log/dmz2-web.log`
    - vse ostalo iz `dmzsrv1` in `dmzsrv2` v `/log/dmz-dump.log`

- v `/var/log/syslog` na `dmzsrv1` in `dmzsrv2` se nebi smelo izpisovati nič
    ```sh
    tail /var/log/syslog
    ```
