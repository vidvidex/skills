# Samba fileshare

## Server

- https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Standalone_Server

- https://www.techrepublic.com/article/how-to-create-a-samba-share-on-ubuntu-server-20-04/

- https://www.cyberciti.biz/faq/samba-user-network-file-sharing-restictions/

- install
    ```sh
    apt install samba
    ```

- v `/etc/samba/smb.conf` dovoli guest access
    ```txt
    [common]
        path = /data/common

        read only = no
        guest ok = yes      # "guest ok" in "guest only" opciji sta napisani en section nižje, lahko kopiraš iz tam
        guest only = yes

        create mask = 0775
        directory mask = 0775

        hosts allow = 10.1.10.0/24
        # valid users = %S # MORA BITI ZAKOMENTIRAN
    ```

- restart
    ```sh
    systemctl restart smbd.service
    ```


## Client



- install
    ```sh
    apt install cifs-utils
    ```

- naredi mountpoint
    ```sh
    mkdir /mnt/documents
    ```

- v `/etc/fstab` dodaj
    ```txt
    //insrv.skills39.local/common   /mnt/documents  cifs    rw,guest,dir_mode=0777,file_mode=0777  0   0
    ```

- mount
    ```sh
    mount -a
    ```

## Testranje

- na clientu mountaš SMB share, če je pravilno mountan (noter vidiš sharane file) potem dela
  ```sh
  mount -a
  ```

- preveriš še, če se pravilno mounta ob rebootu
  ```sh
  reboot
  ```
