# DNS

- https://help.ubuntu.com/community/BIND9ServerHowto

- install
  ```sh
  apt install bind9
  ```

- orodja za preverjanje
  ```sh
  apt install dnstools # nslookup, dig
  apt install net-tools # netstat
  ```

## Osnovna konfiguracija

- v `/etc/bind/named.conf.local` dodaj forward in reverse zone

  ```conf
  zone "example.com" {
          type master;
          file "/etc/bind/db.example.com";
  };

  zone "1.168.192.in-addr.arpa" { # Tukaj prvi trije okteti ipja v obratnem vrstnem redu
          type master;
          file "/etc/bind/db.192"; # Prvi oktet
  };
  ```

- naredi forward in reverse zone file

  ```sh
  cp db.local db.example.com
  cp db.127 db.192
  ```

  - za DDNS naredi `db.example.com` in `db.192` v `/var/lib/bind/` namesto `/etc/bind/`, saj `bind` nima write permissiona za `/etc/bind`

- v `db.example.com` dodaj/spremeni serijsko, domene, dodaj hoste

  ```conf
  ;
  ; BIND data file for local loopback interface
  ;
  $TTL    604800
  @       IN      SOA     ns.example.com. root.example.com. (
                              3           ; Serial
                          604800          ; Refresh
                          86400           ; Retry
                          2419200         ; Expire
                          604800 )        ; Negative Cache TTL
  ;
  @       IN      NS      ns.example.com.
  ns      IN      A       192.168.1.1

  ; Naprej še ostali hosti
  host1      IN      A       192.168.1.2
  host2      IN      A       192.168.1.3
  ```

- v `db.192` dodaj/spremeni serijsko, domene, dodaj reverse route

  ```conf
  ;
  ; BIND data file for local loopback interface
  ;
  $TTL    604800
  @       IN      SOA     ns.example.com. root.example.com. (
                              3           ; Serial
                          604800          ; Refresh
                          86400           ; Retry
                          2419200         ; Expire
                          604800 )        ; Negative Cache TTL
  ;
  @       IN      NS      ns.example.com.
  1       IN      PTR     ns.example.com.

  ; Naprej še ostali hosti
  2      IN      PTR      host1
  3      IN      PTR      host2
  ```

- preveri konfiguracijo

  ```sh
  named-checkzone example.com db.example.com
  named-checkzone 1.168.192.in-addr.arpa db.192
  ```

- restart bind9

  ```sh
  systemctl restart bind9
  ```

- v `/var/log/syslog` lahko pogledaš če so bili vsi zoni pravilno naloženi
  ```sh
  tail /var/log/syslog
  ```


## Master/slave

- na masterju v `/etc/bind/named.conf.local` dodaj `allow-transfer`

  ```conf
  zone "example.com" {
          type master;
          file "/etc/bind/db.example.com";
          allow-transfer { IP_SECONDARY_SERVERJA; }; # Pazi na ; po ipju
  };

  zone "1.168.192.in-addr.arpa" { # Tukaj prvi trije okteti ipja v obratnem vrstnem redu
          type master;
          file "/etc/bind/db.192"; # Prvi oktet
          allow-transfer { IP_SECONDARY_SERVERJA; }; # Pazi na ; po ipju
  };
  ```
  
- na slavu v `/etc/bind/named.conf.local` dodaj `masters`

  ```conf
  zone "example.com" {
          type slave;
          file "/etc/bind/db.example.com";
          masters { IP_PRIMARY_SERVERJA; };
  };

  zone "1.168.192.in-addr.arpa" {
          type slave;
          file "/etc/bind/db.192";
          masters { IP_PRIMARY_SERVERJA; };
  };
  ```
  

## Forwardiranje

- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-caching-or-forwarding-dns-server-on-ubuntu-14-04
- https://unix.stackexchange.com/questions/283276/bind9-denied-query

- za forwardanje samo enega zona v `/etc/bind/named.conf.local` dodaj:
  ```txt
  zone "poddomena.example.com" {
    type forward;
    forward only;
    fowarders { IP_SERVERJA_KAMOR_FORWARDAMO; };
  };
  ```

- za forwardanje vsega (forward-only server) v `/etc/bind/named.conf.options` dodaj:
  ```txt
  forwarders {
    IP_FORWARD_SERVERJA;
  };
  forward only;
  ```
  

- v vsakem primeru v  `/etc/bind/named.conf.options` dodaj:
  ```txt
  allow-query {
    any;
  };

  dnssec-validation no # Je že napisan samo spremeni na "no"
  ```

## Testiranje

- ukazi `dig`, `nslookup` ali `ping`
  ```sh
  dig www.example.net
  nslookup www.example.net
  ping www.example.net      # Napiše kateri IP bo pingal
  ```

- pri `dig` lahko z `@NEK_NASLOV` specificiraš kateri DNS server bo queryjal (brez bo uporabil tistega, ki je konfiguriran v `/etc/resolv.conf`)
  ```sh
  dig @10.0.10.1 www.example.net
  ```

