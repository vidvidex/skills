# Debian

## Dodajanje CDjev z packagi

- v ESXi lahko dodaš vse CDje, preveri da so connected

- dodaj CDje v apt
  ```sh
  apt-cdrom add
  ```

- v `/etc/apt/sources.list` pri vsakem cdju dodaš `[trusted=yes]`

  ```txt
  deb [trusted=yes] cdrom:[Debian GNU/Linux...
  ```

- Refresh
  ```sh
  apt update
  ```


## Spreminjanje hostname

- `hostnamectl set-hostname NEW_HOSTNAME`
- popravi v `/etc/hosts`

## Spreminjanje časovnega pasu

```sh
timedatectl set-timezone Europe/Vienna
```

## Spreminjanje tipkovnice

- spremeni `XKBLAYOUT` v `/etc/default/keyboard`

  ```text
  XKBLAYOUT="us"
  ```

## Spreminjanje omrežja

- ime interfaca dobiš iz `ip a` (ni nujno da je `ens192`)

- **dhcp**: v `/etc/network/interfaces` dodaj

  ```conf
  auto ens192
      iface ens192 inet dhcp
  ```

- **static**: v `/etc/network/interfaces` dodaj

  ```conf
  auto ens192
  iface ens192 inet static
      address 10.0.10.1
      netmask 255.255.255.0
      gateway 10.0.10.254

  iface ens192 inet6 static
      address 2001:a::1
      netmask 64
  ```

- DNS serverji v `/etc/resolv.conf`
  ```sh
  nameserver 8.8.8.8
  ```

- restart networking

  ```sh
  systemctl restart networking
  ```

## Dodajanje dekstop environmenta

```sh
tasksel
reboot
```

## Izklop NTPja (da ne spama sysloga)

- https://askubuntu.com/a/898380
- ```sh
  systemctl stop systemd-timesyncd
  systemctl disable systemd-timesyncd
  ```

## Razno

- terminal browser za branje dokumentacije v HTML:
  ```sh
  apt install w3m
  w3m nekFile.html
  ```

- boljša prilagoditev velikosti zaslona pri linuxih z GUI: 
  ```sh
  apt install open-vm-tools
  ```
