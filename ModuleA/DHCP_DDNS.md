# DHCP + DDNS


## DHCP 

https://wiki.debian.org/DHCP_Server


- install 
  ```sh
  apt install isc-dhcp-server
  ```


- v `/etc/default/isc-dhcp-server` dodaj ime notranjega interfaca (npr. `ens192`) v `INTERFACESv4`

- spremeni `/etc/dhcp/dhcpd.conf` v nekaj takega:
  ```txt
  authoritative;
  option domain-name "skills39.local";
  option domain-name-servers ns.skills39.local;

  ddns-updates on;
  ddns-update-style interim;
  ignore client-updates;

  include "/etc/dhcp/ddns.key";

  zone skills39.local. { # PAZI PIKA
    primary 10.0.20.1; # IP DNS serverja (127.0.0.1 če sta DHCP in DNS na istem)
    key DDNS_UPDATE;
  }

  zone 10.in-addr.arpa. {  # PAZI PIKA
    primary 10.0.20.1;  # IP DNS serverja (127.0.0.1 če sta DHCP in DNS na istem)
    key DDNS_UPDATE;
  }


  subnet 10.1.10.0 netmask 255.255.255.0 {
    range 10.1.10.100 10.1.10.200;
    option routers 10.1.10.254;
  }
  ```

- restart
  ```sh
  systemctl restart isc-dhcp-server
  ```


## DDNS

https://wiki.debian.org/DDNS

- na serverju v `/etc/bind/` naredi ključe

  ```sh
  dnssec-keygen -a HMAC-SHA256 -b 128 -n USER DDNS_UPDATE
  ```

- iz `.private` kopiraj vse po `Key: ` v nov file `ddns.key`:
  ```txt
  key DDNS_UPDATE {
          algorithm HMAC-SHA256;
          secret "pRP5FapFoJ95JEL06sv4PQ=="; # Tu prilepi ključ iz .private
  };
  ```

- kopiraj `ddns.key` v `/etc/dhcp/` in `/etc/bind/` (ni potrebno če si ga tu ustvaril) in spremeni premissione:
  ```sh
  chown root:bind /etc/bind/ddns.key
  chmod 640 /etc/bind/ddns.key

  chown root:root /etc/dhcp/ddns.key
  chmod 640 /etc/dhcp/ddns.key
  ```

- v `/etc/bind/named.conf.local` includaj `ddns.key`, v zone dodaj `allow-update` in spremeni lokacijo `db.*` datotek na `/var/lib/bind` (`bind` ne sme pisati v `/etc/bind/` lahko pa v `/var/lib/bind`):
  ```txt
  include "/etc/bind/ddns.key";

  zone "skills39.local" {
      type master;
      file "/var/lib/bind/db.skills39.local";
      allow-update { key DDNS_UPDATE; };
  };

  zone "10.in-addr.arpa" {
      type master;
      file "/var/lib/bind/db.10";
      allow-update { key DDNS_UPDATE; };
  };
  ```

- restart 
  ```sh
  systemctl restart bind9
  ```


## Testiranje

- iz DHCP clienta zahtevaš IP naslov, npr. z restartom `networking`
  ```sh
  systemctl restart networking
  ```

- pogledaš če si dobil IP iz definiranega območja

- z  `dig`, `nslookup` ali `ping` preveriš če je client dodan v DNS
  ```sh
  dig dhcp-client.devices.example.net
  ping dhcp-client.devices.example.net    # Napiše kateri IP bo pingal
  ```
