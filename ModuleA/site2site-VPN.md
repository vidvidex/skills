# Libreswan site2site vpn

- https://libreswan.org/wiki/Host_to_host_VPN

- https://libreswan.org/wiki/Subnet_to_subnet_VPN

ukazi:

```txt
ipsec --help
```


- Install
  ```sh
  apt install libreswan
  ```

- Naredi ključe
  ```sh
  ipsec initnss
  ipsec newhostkey --output /etc/ipsec.d/NAME.secrets
  ipsec showhostkey --left|--right --ckaid CKAID_OD_PREJŠNE_KOMANDE # (na enem serverju daš left, na drugem right)
  ```

- kopiraj sample config file (configa na obeh serverjih sta enaka, naredi na enem in kopiraj na drugega)

  ```sh
  cp /usr/share/doc/libreswan/examples/linux-linux.conf ipsec.conf
  ```

- v `ipsec.conf` spremeni `left`, `right`, `leftsubnet` in `rightsubnet`, zakomentiraj vrstice s ključi in dodaj `authby=rsasig`

- prilepi ključ iz `showhostkey` ukaza

- dodaj tunnel (default ime je `linux-to-linux`)

  ```sh
  ipsec setup start
  ipsec auto --add IME_TUNELA_IZ_IPSEC_CONF
  ipsec auto --up IME_TUNELA_IZ_IPSEC_CONF
  ```

- dovoli v filewallu (potrebno zaobiti NAT in dovoliti ipsec)

- https://libreswan.org/wiki/FAQ#How_do_I_configure_my_firewall_to_allow_IPsec
- https://libreswan.org/wiki/FAQ#NAT_.2B_IPsec_is_not_working

  ```sh
  chain input {
    type filter hook input priority 0; policy drop;

    ip saddr 100.43.60.66 accept; # Dovoli vse kar pride iz druge strani (lažje (a manj varno) kot dovoljenje posameznih portov)
  }
  ```

  ```sh
  chain postrouting {
    type nat hook postrouting priority 100;

    ip addr 10.0.0.0/8 accept; # Ne delaj NATa na lokalnih omrežjih (s tem NAT ne do oviral IPsec)
    oifname "ens192" masquerade;
  }
  ```


- Delujoč config:
  ```conf
  conn s2s
    left=100.43.60.66
    leftsubnet=10.1.10.0/24
    leftid=@hqfw
    leftrsasigkey=RSA_KLJUČ_LEFT
    leftsourceip=10.1.10.254

    right=100.43.60.65
    rightsubnet=10.0.0.0/16
    rightid=@cloudfw
    rightrsasigkey=RSA_KLJUČ_RIGHT
    rightsourceip=10.0.10.254

    authby=rsasig
  ```

## Testiranje

- z `traceroute` pogledaš, če gre promet čez tunel
  ```sh
  traceroute naprava-na-drugi-strani.com
  ```
- pogledaš koliko packetov je šlo čez VPN
  ```sh
  ipsec trafficstatus
  ```
