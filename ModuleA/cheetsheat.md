# Linux Cheatsheet

## Struktura ukazov

- ponavadi so sestavljeni iz ene besede (ukaza), ki mu sledijo flagi (te zadeve z -) in parametri
    - npr `ls -l /home/vid`, `ls` je ukaz, `-l` je flag, `/home/vid` je parameter na katerem ukaz deluje
    - velika večina ukazov ima za vsako stvar (ali pa vsaj za večino) kratek in dolg flag, npr. `-d` in `--delete`. Dolgi flagi imajo skoraj vedno 2 minusa. To nam omogoča da lahko stackamo kratke flage in se ne bodo zamešali za dolgega: `ls -lah /home/vid` je enako kot `ls -l -a -h  /home/vid` samo krajše
    - pogosto, ne pa vedno, lahko daš flage v poljubnem vrstem redu in na poljubno mesto, tudi po parametru

## Getting help

- pogosto deluje `nek_program -h` ali `nek_program --help`
- bolj obširna dokumentacija je dostopna z `man nek_program` (man = manual)
    - na teh debianih po defaultu ni naložen: `apt install man manpages`
- večina programov shrani kakšna navodila, default konfiguracije in vnaprej napisane sample v `/usr/share/doc/ime_programa/`
    - format teh navodil je precej različen, velikokrat so navadni text fili, včasih pa tudi HTML, markdown ali kaj trejega
    - včasih so compressani (imajo končnico `.gzip`, `.gz` ali kaj podobnega)

## Networking 

- `ping`
    - `ctrl+C` da konča

- `traceroute`

- `ip`
    - `ip address`
    - `ip route`
    - `ip` ukaz se ubnaša kot Cisco ukazi, namesto `ip address` lahko napišeš le `ip a`
    - starejša verzija je `ifconfig`, na nekarih linuxih imaš naloženo oboje, na teh debianih pa je samo `ip`
    - lahko se uporablja tudi za nastavljanje ip naslovov a se po rebootu zbrišejo zato jih je bolje nastavljati v `/etc/network/interfaces`

- `netstat -tnlp`
    - pogledaš na katerih portih posluša kateri program
    - uporabno če rabiš izvedeti katere porte dovoliti na firewallu

## File manipulation

- `ls`
    - kot `dir` na Windowsih
    - `-l` izpiše več info
    - `-a` izpiše tudi hidden file
    - `-h` izpiše velikost v human-readable formatu (npr. 5M namesto samo število bytov)

- `pwd`
    - "print working directory"
    - pove v kateri mapi se trenutno nahajaš

- `mkdir`
    - "make directory"

- `rm`
    - remove
    - če hočeš brisati mape: `rm -r`   

- `cp` 
    - copy

- `mv`
    - move
    - uporablja se tudi za preimenovanje: `mv staro_ime novo_ime`

- `nano`
    - basic text editor

- `cat`
    - izpiše cel file na konzolo, uporaben če rabiš na hitro pogledat kaj je v datoteki

- `less`
    - za gledanje večjih datotek. Če je datoteka velika pri izpisu z `cat` ne boš videl cele ker na linuxih brez gui terminal nima scrollbara
    - velikokrat lahko namesto `less` uporabiš kar `nano`

- `tail`
    - izpiše zadnjih N vrstic datoteke (po defaultu se mi zdi da 10)
    - uporaben za branje log filov, ki so zelo dolgi, tebe pa pravzaprav zanima samo kar je na koncu
    - `tail -n 100 nek_file` za izbiro koliko vrstic ti izpiše
    - `tail -f nek_file` bo izpisal zadnje 10 vrstic in nato čakal da se v file napiše še kaj in izpisal tudi to.

- `grep`
    - iskanje po datotekah
    - npr. `grep DHCP /var/log/syslog` bo izpisal vse vrstice, ki vsebujejo besedo "DHCP" iz datoteke `/var/log/syslog` (log datoteke za celoten sistem)
    - namesto tako kot zgoraj bi lahko to napisal tudi tako: `cat /var/log/syslog | grep DHCP` (z `cat` izpišeš cel file, nato pa ga z `|`preusmeriš iz konzole v nek drug ukaz, v tem primeru `grep`)
    - če hočeš izločiti vse vrstice, ki vsebujejo neko besedo/znak: `grep -v DHCP nek_file` izpiše vse vrstice razen tistih, ki vsebujejo besedo "DHCP"


## User management

- vsak user in skupija ima svoj id (`uid` in `gid`). 
    - navadni uporabniki imajo ponavadi 1000, 1001, ...
    - root ima vedno 0
    - razni sistemski uporabniki imajo ponavadi nekje med 0 in 1000
 

- `useradd` in `adduser`
    - dodajanje uporabnikov, 1. te bo vprašal za razne info o uporabniku, 2. pa samo naredil uporabnika in končal.
    - ponavadi ko narediš uporabnika se hkrati naredi še group z enakim imenom (pri `useradd` se naredi avtomatsko)

- `userdel`
    - brisanje uporabnikov

- `chown`
    - spremeni lastnika datoteke: `chown vid:vid nek_file` (prvi vid je owner, drugi vid pa group, tega lahko velikokrat izpustiš)

- `chmod`
    - spremeniš permissione za file (te pogledaš z `ls -l`)
    - vsak file/directory ima read, write in execute permission za ownerja, group in vse ostale
    - lahko numerično ali s simboli
    - https://chmodcommand.com/


## Programi in storitve

- `apt`
    - package manager za debian, ubuntu in še nekaj podobnih linuxov
    - nalaganje programa: `apt install nekaj`
    - brisanje programa: `apt remove nekaj` (lahko dodaš še `--purge` da pobriše vse config file in podobne datoteke, ki bi drugače ostale na računalniku)
    - včasih na internetu vidiš `apt-get` namesto `apt`, to je praktično enaka stvar, le malce starejša in 4 znake daljša
    - na običajnih linuxih se ponavadi pred `apt install` in `apt upgrade` požene še `apt update` (`update` pogleda katere so najnovejše verzije naloženih programov, `upgrade` pa jih naloži). Na naših virtualkah `update` in `upgrade` ni potrebno uporabljati ker so programi na CDjih in se nikoli ne bodo spremenili

- service/storitev je program, ki deluje nekje v ozadju (npr. DHCP manager, nikoli ga ne vidiš ampak nekje zadaj skrbi za to da imaš IP)

- `systemctl`
    - `systemctl start nekaj.service` in `systemctl stop nekaj.service`
        - prižge/ugasne service, ne obdrži se po restartu
    - `systemctl enable nekaj.service` in `systemctl disable nekaj.service`
        - poskrbi da se service samodejno prižge ob bootu
    - `systemctl restart nekaj.service` 
    - `systemctl status nekaj.service`
    - `.service` končnice ni potrebno pisati

- zagon lastnega programa/skripte:
    - moraš imeti execute permission (`chmod +x program`)
    - `./program` ali `bash program` če na vrh nisi dodal `#!/bin/bash`
