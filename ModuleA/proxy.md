# Proxy

- https://workaround.org/squid-ldap/

## Squid proxy server

- install
    ```sh
    apt install squid
    ```
- v `/etc/squid/squid.conf` dodaj:
    ```txt
    auth_param basic program /usr/lib/squid/basic_ldap_auth -b dc=skills39,dc=local
    -D cn=admin,dc=skills39,dc=local -w Passw0rd! -f uid=%s intsrv.skills39.local

    acl ldap_auth proxy_auth REQUIRED
    http_access allow ldap_auth
    ```
- restart
    ```sh
    systemctl restart squid
    ```

## Firefox

- pod Preferences > Network Settings dodaj proxy:
    ```txt
    HTTP Proxy: hqfw.skills39.local
    Port: 3128
    ```

## Testiranje

- iz clienta, ki je za proxyjem greš na nek website, če te vpraša za username in password, potem dela.

- na web serverju lahko v `/var/log/apache2/access.log` pogledaš iz katerega naslova je prišel request (mora biti IP proxyja)
    ```sh
    cat /var/log/apache2/access.log
    ```
