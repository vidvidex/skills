# OpenVPN Remote Access VPN

- https://openvpn.net/community-resources/how-to/

- naloži na server in client
  ```sh
  apt install openvpn
  ```

## Certifikati

- na CA (root ali intermediate, odvisno od navodil) naredi certifikate za server in client
  ```sh
  ./easyrsa build-server-full server nopass # tu je ime serverja "server" ker je to default v openvpn server configu in je manj dela s preimenovanjem
  ./easyrsa build-client-full client1 nopass # tako naredi certifikate za vsakega uporabnika
  ./easyrsa gen-dh
  ```

- na serverju naredi `ta.key` (neka TLS zadeva). Ta ukaz je napisan v `server.conf` pri `tls-auth ta.key 0`
  ```
  openvpn --genkey --secret ta.key
  ```


- Na cliente v `/etc/openvpn` kopiraj:
  - `client1.crt` in `client1.key` (načeloma bi moral key narediti na clientu in poslati sign request na server, ga podpisati in poslati nazaj a je tako mnogo hitreje)
  - `ta.key`

- Na server v `/etc/openvpn` kopiraj:
  - `server.crt` in `server.key`
  - `dh.pem`
  - `ta.key` je že na serverju ker smo ga tu naredili

- Na server in cliente kopiraj `ca.crt` (če so certifikati narejeni na intermediate CA potem morata biti v `ca.crt` crt od root CA in intermediate CA, baje je vrsti red pomemben)
    ```txt
    ------BEGIN CERTIFICATE------

          [ root CA crt ]

    ------END CERTIFICATE------
    ------BEGIN CERTIFICATE------

      [ intermediate  CA crt ]

    ------END CERTIFICATE------
    ```
  
## Server

- kopiraj in razširi sample server.conf

  ```sh
  cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/

  gunzip server.conf.gz
  ```

- `ca.crt`, `dh.pem`, `server.key`, `server.crt` in `ta.key` so zraven `server.conf`

- spremembe v `server.conf`:
  - poglej da so imena in poti do certifikatov pravilni (predvsem `dh.pem`)
  - oglašuj omrežja, dostopna preko VPNja
    ```txt
    push "route 10.0.10.0 255.255.255.0"
    push "route 10.0.20.0 255.255.255.0"
    push "route 10.1.10.0 255.255.255.0"
    ```
- zagon:
  - za testiranje
    ```sh
    openvpn server.conf
    ```
  - autostart (poišče vse `.conf` file v `/etc/openvpn`)
    ```sh
    systemctl enable openvpn
    ```

- dovoli `UDP 1194` v firewallu

  ```txt
  udp dport 1194 accept;
  ```



## Client

- kopiraj `ca.crt`, `ta.key`, `client.key` in`client.crt` iz serverja kjer si jih podpisal

- zraven teh kopiraj še sample config za client

  ```sh
  cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/
  ```

- v `client.conf` poglej da so imena in pot certifikatov pravilni

- napiši IP serverja pod `remote`

- zagon:
  - za testiranje
    ```sh
    openvpn client.conf
    ```
  - autostart (poišče vse `.conf` file v `/etc/openvpn`)
    ```sh
    systemctl enable openvpn
    ```

## Testiranje

- preden uporabiš/enablaš sevice `openvpn` lahko konfiguracijo testiraš z ukazom `openvpn`
  ```sh
  openvpn server.conf
  openvpn client.conf
  ```
- pogledaš če je client dobil IP iz poola naslovov za VPN
  ```sh
  ip a
  ```
- pokusiši pingati iz VPN clienta v oddaljeno omrežje in obratno
  ```sh
  ping naprava-v-omrezju.com    # VPN client -> Naprava v oddaljenem omrežju
  ping 10.8.0.6                 # Naprava v oddaljenem omrežju -> VPN client
  ```
- namesto `ping` lahko uporabiš tudi `traceroute`, da vidiš točno pot po kateri packet potuje (da gre res čez tunel in ne okoli)
