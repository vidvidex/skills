# Apache2 webserver

## Basic http config

- naloži `apache2`

  ```sh
  apt install apache2
  ```

- če je potrebno popravi/spremeni `/var/www/html/index.html`
  - če želiš sprazniti file:
    ```sh
    echo "" > index.html
    ```

- če je potrebnih več spletnih strani v `/etc/apache2/sites-available` kopiraj default config file in naredi novega za vsak website. Config file se mora končati z `.conf`

  ```sh
  cp 000-default.conf example.net.conf
  ```

- v website config file (`000-default.conf` oz npr. `example.net.conf`) dodaj ServerName s primarno domeno spletne strani (dodatne domene se lahko doda z `ServerAlias`) in po potrebi spremeni `DocumentRoot`. Če je potrebno dodaj še redirect na https
  ```txt
  ServerName example.net
  ServerAlias www.example.net
  DocumentRoot /var/www/example.net

  Redirect / https://example.net
  ```

  - če je `DocumentRoot` izven `/var/www` (v spodnjem primeru je `/data/intranet`) v `/etc/apache2/apache2.conf` odkomentiraj sample podoben spodnjemu in ga popravi da bo pravilen (spremeni path)
    ```
    <Directory /data/intranet>
        Options Indexes FollowSymLinks
        AllowOverride None  
        Require all granted
    </Directory>
    ```

- če ne uporabljaš default config fila (`000-default.conf`) omogoči nov config file

  ```sh
  a2ensite example.net # za example.net.conf
  systemctl restart apache2
  ```

## SSL

- naredi certifikat (easyrsa ali openssl)

  ```sh
  ./easyrsa build-server-full NAME nopass
  ```

- v `/etc/apache2/sites-available` naredi ssl config file

  ```sh
  cp default-ssl.conf example.net-ssl.conf
  ```

- mogoče je potrebno enablat `ssl` modul

  ```sh
  a2enmod ssl
  ```

- v `default-ssl.conf` naredi vse spremembe kot za navaden http website config ter popravi pot do generiranih certifikatov (.key in .crt).
  ```txt
  ServerName example.net
  ServerAlias www.example.net
  DocumentRoot /var/www/example.net
  ```

- restart apache2
  ```sh
  systemctl restart apache2
  ```

### Testiranje

- na clientu z GUIjem v Firefoxu greš na `www.example.net` in pogledaš če je prikazan custom `index.html`

- če nimaš GUIja uporabiš `curl` ali `wget`
  ```sh
  curl www.example.net  # Ta po defaultu page izpiše na konzolo
  wget www.example.net  # Ta po defaultu shrani page na disk
  ```
  - testiranje HTTPS strani:
    - nujno napisati protokol `https://`
    - `curl` flag `-k` ignorira SSL napako zaradi nezaupanja certifikatu (če host ne zaupa CAju)
      ```sh
      curl -k https://www.example.net
      ```
  - testiranje IPv6:
    - `curl` flag `-6` bo uporabil le IPv6
      ```sh
      curl -6 ipv6-only.example.net
      ```
      

## WebDAV
- https://www.digitalocean.com/community/tutorials/how-to-configure-webdav-access-with-apache-on-ubuntu-20-04

- https://wiki.archlinux.org/title/Davfs2

### Server
- omogoči module

  ```sh
  a2enmod dav_fs
  ```

- v website config file (http in https) znotraj VirtualHost dodaj:

  ```conf
  Alias /webdav /var/www/html/webdav
  <Location /webdav>
      DAV On
  </Location>
  ```

- naredi direktorij za webdav
  ```sh
  mkdir /var/www/html/webdav
  chown www-data:www-data /var/www/html/webdav -R
  ```

- restart apache2

### Client

- naloži `davfs2`
  ```sh
  apt install davfs2
  ```
- v `/etc/davfs2/secrets` pod credentials dodaj
  ```sh
  /webdav hans  Passw0rd!
  ```

- uporabnik mora biti v skupini `davfs2`
  ```sh
  usermod -aG davfs2 hans
  ```

- dodaj v `fstab`:
  ```sh
  https://example.net/webdav /webdav davfs rw,user,uid=hans,_netdev 0 0 # zaradi _netdev počaka na network preden poskuša mountat
  ```

- za https mora linux zaupati CA, glej `ca.md`

### Testranje

- na clientu mountaš WebDAV share, če je pravilno mountan (noter vidiš sharane file) potem dela
  ```sh
  mount -a
  ```

- preveriš še, če se pravilno mounta ob rebootu
  ```sh
  reboot
  ```



## LDAP

https://gist.github.com/chrisglass/634089

https://httpd.apache.org/docs/2.4/mod/mod_authnz_ldap.html

- primer konfiguracije:
  - naloži `apache2-doc` in terminal browser (npr `w3m`)
    ```sh
    apt install apache2-doc w3m
    ```
  - primer je v `/usr/share/doc/apache2-doc/manual/en/mod/mod_ldap.html` (prava dokumentacija za `authnz_ldap` je v  `/usr/share/doc/apache2-doc/manual/en/mod/mod_authnz_ldap.html` a nima primera, izgleda pa skoraj enako, le nekaj directivov manjka)


- enable module
  ```sh
  a2enmod authnz_ldap
  ```

- v website config file dodaj:
  ```txt
  <Location />
    AuthType Basic
    AuthBasicProvider ldap

    Requre valid-user
    AuthName "LDAP auth" # TO SE PRIKAŽE UPORABNIKU

    AuthLDAPURL "ldap://127.0.0.1/dc=skills39,dc=local"
    AuthLDAPBindDN "cn=admin,dc=skills39,dc=local"
    AuthLDAPBindPassword "Passw0rd!"
  </Location>
  ```

### Testiranje

- v Firefoxu greš na website, vpraša te za username in password