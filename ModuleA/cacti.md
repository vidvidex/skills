# Cacti/SNMP

## Server

- install
    ```sh
    apt install cacti
    ```

- webui je na `cloudfw.skills39.local/cacti`, username `admin`, password enak kot za mysql (`Passw0rd!`)

- ne pozabi dovoliti port 80 na firewallu

- dodaj `dmzsrv1` in `dmzsrv2` kot Net-SNMP device

- glej da uporabiš SNMPv3 z authPriv

- graph za memory usage že obstaja zato ne bo dovolil narediti novega. Le izbereš ga ter ga dodaš na default tree

### Testiranje

- pogledaš če se na grafu začne risati memory usage


## SNMP agent na clientih

- https://support.itrsgroup.com/hc/en-us/articles/360020056114-How-to-configure-a-Linux-server-for-SNMP-monitoring

- install
    ```sh
    apt install snmpd
    ```

- dodaj userja
    - ustavi `snmpd` service 
        ```sh
        systemctl stop snmpd.service
        ```
    - po navodilih v `/etc/snmp/snmpd.conf` v `/var/lib/snmp/snmpd.conf` dodaj
        ```txt
        createUser authPrivUser SHA Passw0rd! AES Passw0rd!
        ```
    - ponovno zaženi `snmpd` service 
        ```sh
        systemctl start snmpd.service
        ```
    - v `/var/lib/snmp/snmpd.conf` je prej dodana vrstica izginila, spodaj je nekaj takega: `usmUser 1 3 0x80001f88801fe67e4b048e4d5500000000 0x6b616b6100 0x6b616b6100 NULL .1.3.6.1.6.3.10.1.1.2 0xcab3cb478072eef2df19c0403f030678 .1.3.6.1.6.3.10.1.2.4 0x0f6c0d5d2e521c53630039b1f04354d8 0x`
    
- v `/etc/snmp/snmpd.conf` dovoli remote access (vse ali samo iz `cloudfw`)
    ```txt
    agentAddress udp:161,udp6:[::1]:161
    ```
    in daj uporabniku `authPrivUser` read permission:
    ```txt
    rouser  authPrivUser
    ```
    
    *(obe nastavitvi sta že v filu, le spremeniti ju je treba)*
