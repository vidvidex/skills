# CA setup

## Easy-RSA

- **preprostejše in hitrejše kot OpenSSL**

## Osnovna uporaba

- Naloži
    ```sh
    apt install easy-rsa
    ```

- Kopiraj easyrsa v `ca` direktorij (če je zahtevano)
    ```sh
    cp /usr/share/easy-rsa/* /ca -r
    ```
- Spremeni/popravi `vars`
    ```
    cp vars.example vars
    ```
    - nastavi EASYRSA_DN na "org", brez tega v certifikatu ne bo vseh potrebih opcij 
    - popravi ostale potrebne opcije, nepotrebne pusti prazne ("")

- Postavi CA
    ```sh
    ./easyrsa init-pki
    ./easyrsa build-ca
    ```

- Naredi certifikat:
    ```sh
    ./easyrsa build-server-full FQDN_SERVERJA nopass
    ALI
    ./easyrsa build-client-full FQDN_CLIENTA nopass
    ```
    - ime certifikata je `FQDN.key` oz `FQDN.crt`
    - `.key` so v `pki/private/`
    - `.crt` so v `pki/issued/`

### SAN

- https://github.com/OpenVPN/easy-rsa/issues/22#issuecomment-407164873

- docs:
    ```sh
    ./easyrsa help options
    ./easyrsa help altname
    ```

- naredi cert s SAN
    ```sh
    ./easyrsa --subject-alt-name="DNS:neki.example.net,DNS:drugi.example.net,IP:32.54.24.24" build-server-full test1.example.net nopass
    ```
    *`--subject-alt-name` mora biti na začetku, pred FQDN in `build-server-full`*


### Intermediate CA
- https://github.com/OpenVPN/easy-rsa/issues/190

- initialise intermediate CA
    ```sh
    ./easyrsa init-pki
    ./easyrsa build-ca subca # Ne naredi ca.crt, le csr v pki/reqs/ca.csr
    ```
- prenesi `ca.csr` na root CA v `pki/reqs` directory
- podpiši `ca.csr`
    ```sh
    ./easyrsa sign-req ca ca # prvi ca je ime certifikata, drugi pa tip
    ```
- kopiraj `pki/issued/ca.crt` nazaj na intermediate CA v `pki` direktorij



## OpenSSL

**Daljši ukazi, ki si jih je težko zapomniti**

### CA

- naredi key
    ```sh
    openssl genrsa -out ca.key 4096
    ```
- naredi crt
    ```sh
    openssl req -new -x509 -key ca.key -out ca.crt
    ```

### Klienti

- naredi key
    ```sh
    openssl genrsa -out client.key 4096
    ```

- naredi csr
    ```sh
    openssl req -new -key client.key -out client.csr
    ```

- naredi crt
    ```sh
    openssl x509 -req -days 1000 -in client.csr -CA ca.crt -CAkey ca.key -out client.crt -CAcreateserial
    ```


## Dodaj CA v Linux

- https://superuser.com/questions/437330/how-do-you-add-a-certificate-authority-ca-to-ubuntu

- kopiraj `ca.crt` v `/usr/local/share/ca-certificates` in poženi `update-ca-certificates`

- Firefox in Thunderbird imata svoj certificate store, dodaj preko gui


