# IIS

## Double escaping

- https://docs.microsoft.com/en-us/iis/manage/configuring-security/configure-request-filtering-in-iis#to-configure-general-request-filter-options-by-using-the-ui

- v server managerju > `Tools` > `Internet Infomation Services (IIS) Manager` > v levem drevesu izbereš nivo, npr. `Default Web Site` > 2x klikneš na `Request Filtering` > desno v Actions panelu klikni `Edit Feature Settings...` > `Allow double escaping`

## Directory browsing

- v server managerju > `Tools` > `Internet Infomation Services (IIS) Manager` > v levem drevesu izbereš nivo ali nek dictory > 2x klikneš na `Directory Browsing` > desno v Actions panelu klikni `Enable`
