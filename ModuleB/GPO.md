# GPO

```bash
gpupdate /force
```


## Disable Recycle Bin on desktop

- https://www.groovypost.com/howto/microsoft/xp/remove-the-windows-recycle-bin-from-your-desktop/

- `User configuration / Administrative Templates / Desktop / Remove Recycle Bin icon from desktop`

## Disable first sign-in animation

- https://www.windowscentral.com/how-disable-first-sign-animation-windows-10

- `Computer Configuration / Administrative Templates / System / Logon / Show first sign-in animation`

## Active hours

- https://docs.microsoft.com/en-us/windows/deployment/update/waas-restart#configuring-active-hours-with-group-policy

- `Computer Configuration / Administrative Templates / Windows Components / Windows Update / Turn off auto-restart for updates during active hours`

## NTP

- https://theitbros.com/configure-ntp-time-sync-group-policy/

## Root CA certificate distribution

- https://vmlabblog.com/2019/09/setup-server-2019-enterprise-ca-4-5-setup-group-policy/

- `Computer Configuration / Policies / Windows Settings / Security Settings / Public Key Policies` > desen klik na `Trusted Root Certification Authorities` > `Import`

    - v Browse dialogu pojdi na `\\WINSRV9` in najdi Root CA cert
    
    - cert shrani v default store (Trusted Root Certification Authorities)

## Auto enrollment

- tu piše da rabimo Group Policy Management Editor, zakaj? https://docs.microsoft.com/en-us/windows-server/networking/core-network-guide/cncg/server-certs/configure-server-certificate-autoenrollment

- `User Configuration / Policies / Windows Settings / Security Settings / Public Key Policies / Certificate Services Client - Auto-Enrollment` > enable in označi oba checkboxa

## Digitally encrypt domain traffic

- https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/domain-member-digitally-encrypt-or-sign-secure-channel-data-always

- `Computer Configuration / Windows Settings / Security Settings / Local Policies / Security Options` > enable:
    - `Domain member: Digitally encrypt or sign secure channel data (always)`
    - `Domain member: Digitally encrypt secure channel data (when possible)`
    - `Domain member: Digitally sign secure channel data (when possible)`

## Make members of group local admins

- `Compututer Configuration / Preferences / Control Panel Settings / Local users and groups`
    - v desnem oknu desen klik > `New` > `Local group`
    - action pusti `Update`
    - v Group name daj `Administrators`
    - spodaj pri Members klikni `Add...` in izberi group, ki ga hočeš dodati v skupino Administrators

## Dovoli login le določeni groupi

- https://serverfault.com/questions/360029/gpo-allow-log-on-locally-admins-access-only

- `Computer Configuration / Policies / Windows settings / Security Settings / Local policies / User Rights Assignment / Allow log on locally`
    - označi `define these policy settings`
    - dodaj group `Administrators`

## Onemogoči batch-job logon
https://docs.microsoft.com/en-us/internet-explorer/ie11-deploy-guide/auto-configuration-settings-for-ie11
- https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-r2-and-2012/dn221958(v=ws.11)

-  `Computer Configuration / Policies / Windows settings / Security Settings /  Local policies / User Rights Assignment / Deny log on as a batch job`
    - označi `define these policy settings`
    - dodaj group `Administrators`

## Credential guard

- https://docs.microsoft.com/en-us/windows/security/identity-protection/credential-guard/credential-guard-manage

- `Computer configuration / Policies / Administrative Templates / System / Device Guard / Turn on Virualization Based Security`
    - pri `Cretedential Guard Configuration` izberi `Enabled without lock`


## Disable IE auto configurations

- https://docs.microsoft.com/en-us/internet-explorer/ie11-deploy-guide/auto-configuration-settings-for-ie11#locking-your-automatic-configuration-settings ???

## RDS disconnect after X min

- `Computer Configuraton / Policies / Administrative Templates / Windows Components / Remote Desktop Services / Remote Desktop Session Host / Session Time Limits / Set time limit for active but idle Remote Desktop Services sessions`