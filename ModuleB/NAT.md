# Routing 

- https://www.experiencingit.net/windows/windows-server/windows-server-2016-nat-router/


## Brez VPNja

- naloži `remote Access` role in izberi `Routing` role service

- v `Routing and Remote Access` desen klik na ime serverja > `Configure and Enable Routing and Remote Access`

- izberi `NAT`

- izberi public (WAN) interface (tu pomaga če si jih preimenoval v kaj bolj smiselnega kot je default)


## Z VPNjem

- ker je VPN že postavljen `Configure and Enable Routing and Remote Access` wizard ni na voljo

- odpri `Routing and Remote Access`

- v desnem meniju razširi `IPv4` in desen klik na `NAT` > `New interface`
    - izberi LAN interface
    - pusti `Private interface connected to private network`

- še enkrat desen klik na `NAT` > `New interface`
    - tokrat izberi WAN interface
    - izberi `Public interface connected to the Internet` in označi checkbox `Enable NAT on this interface`

## Port forwarding

- v `Routing and Remote Access` pri `NAT` desen klik na WAN interface > `Properties` > `Services and Ports`
    - označi `Secure Web Server (HTTPS)` in mu dodaj private address (IP tistega serverja na katerega pošiljaš promet)