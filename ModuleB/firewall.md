# Firewall

## Dovoli ping

- https://manage.accuwebhosting.com/knowledgebase/2609/How-to-Allow-PingorICMP-Echo-Request-in-Windows-Firewall.html

- odpiri `Windows Defender Firewall`
- na levi klikni `Advanced Settings`
- pod Inbound rules na desni klikni `New rule...`
- rule type daj `Custom`
- pusti `All programs`
- izberi protokol `ICMPv4` (ali `ICMPv6` če delaš IPv6)
- specificiraj lokalne in remote IP naslove za katere naj ta rule velja (za ICMP ponavadi le remote IPje iz katerih dovoliš ping)
- pusti `Allow the connection`
- pusti da velja za domeno, privatno in javno omrežje
- poimenuj ta rule
- testiraj z `ping`


## Dovoli NTP

- `UDP 123`


## Import .wfw

- odpiri `Windows Defender Firewall`
- na levi klikni `Advanced Settings`
- zgoraj klikni `Action` > `Import Policy`
