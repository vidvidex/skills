# CA

Je to distribution folder? https://www.cisco.com/c/en/us/support/docs/security/identity-services-engine/216528-configure-microsoft-ca-server-to-publish.html


- sub CA moraš biti loginan kot domain admin da lahko narediš Enterprise CA, pa mogoče rabiš to https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2012-R2-and-2012/dn722303(v=ws.11)?redirectedfrom=MSDN


## Root CA

-  https://stealthpuppy.com/deploy-enterprise-root-certificate-authority/

- ni v domeni (ker se ga itak ugasne)

### Install

- naloži `Active Directry Certificate Services`
    - pod role services izberi samo `Certification Authority` (je default)

- zaženi AD CS configuration (tista zadeva ki jo ponudi po tem ko se role naloži)

- prijavi se kot lokalni Administrator

- izberi `Certification Authority` (edina opcija, ki je na voljo)

- izberi `Standalone CA` (Enterprise ni na voljo ker server ni v domeni)

- izberi `Root CA`

- `Create a new private key`
    - defaulti so ok

- napiši Common name za CA

- izberi validity (default = 5 let)

- pusti default database location

- klikni `Configure`

### Dodatna konfiguracija

- Server Manager > Tools > Certification Authority

- desni klik na common name CAja v drevesu na levi > Properties (mogoče traja nekaj sekund da se okno pojavi)

- za CDP in AIA extension (dropdown izbira zgoraj) naredi:

    - pod Extensions na clipboard kopiraj `http:...`

    - klikni na `Add...`
        - v Location prilepi tisto `http:...` zadevo
        - spremeni `<ServerDNSName>` v ime Sub CA serverja (`WINSRV9.intra.skill39.at`)
        - klikni `Ok`
        - za novo dodano opcijo izberi vse `Include ...` checkboxe spodaj (načeloma se mi zdi da je pomemben samo prvi)

    - klikni `Ok` in dovoli da se AD CS restarta

    - počakaj da se AD CS restarta

- v levem drevesu desen klik na `Revoked Certificates` > `Properties`, izberi še `Publish Delta CRLs`

- v levem drevesu desen klik na `Revoked Certificates` > `All tasks` > `Publish`
    - po nekaj sekundah se pojavi okno, ki sprašuje o tipu CRl, default (`New CRL`) je ok

- certifikati so v `C:\Windows\System32\CertSrv\CertEnroll`

## Sub CA

- https://stealthpuppy.com/deploy-enterprise-subordinate-certificate-authority/

- je v domeni

### Install

- naloži `Active Directry Certificate Services`
    - pod role services izberi `Certification Authority` in `Certification Authority Web Enrollment`

- zaženi AD CS configuration (tista zadeva ki jo ponudi po tem ko se role naloži)

- prijavi se kot domain Administrator

- izberi `Certification Authority` in `Certification Authority Web Enrollment`

- izberi `Enterprise CA`(zato je potreben domain Administrator)

- izberi `Suborbinate CA`

- `Create a new private key`
    - defaulti so ok

- napiši Common name za CA

- shrani certificate request na target machine, lahko pa tudi izbereš zgornjo opcijo in izbereš root CA server, kar bo certificate sign request samo poslalo na root CA in ti tako prihranilo kopiranje v eno smer.

- pusti default database location

- klikni `Configure`

### Podpis Sub CA certifikata na Root CA

- iz Sub CA na Root CA kopiraj request (npr. z RDP)

- desen klik na common name v levem meniju > `All tasks` > `Submit new request` > izberi req file iz Sub CA

- po nekaj refreshih se req iz Sub CA pojavi pod `Pending Requests`

- desen klik na request > `All tasks` > `Issue`

- pod `Issued Certificates` desen klik na cert > `Open` > `Details` > `Copy to File...`
    - pod format izberi `Cryptographic Message Syntax Standard - PKCS #7 Certificates` in označi checkbox spodaj

- exportaj nekam kjer boš potem to našel

- kopiraj ta crt nazaj na Sub CA, hkrati iz Root CA na Sub CA (v isto mapo) kopiraj CRL file iz `C:\Windows\System32\certsrv\CertEnroll`

- desen klik na common name > `All tasks` > `Install CA certificate` > Najdi podpisan Sub CA certifikat iz Root CA 
    - če so CRL poti narobe nastavljene bo izpisal error

- desen klik na common name > `All tasks` > `Start service`



### IIS

- **to je najbrž narobe**

- naredi nov Virtual Directory v IIS in ga mappaj na "distribution folder" (karkoli to je)

- za ta nov virtual directory enablaj directory listing in double escaping (glej [IIS.md](IIS.md))

- v virtual directory kopiraj vsebino `C:\Windows\System32\certsrv\CertEnroll` (je to res najboljši način?)


## Key recovery agent

- https://www.itprotoday.com/active-directory/enabling-key-recovery-active-directory-certificate-services
- https://social.technet.microsoft.com/Forums/lync/en-US/3dd3472a-dac0-4016-980c-9c16a06dcc33/issue-certificate-from-ca-server?forum=winserversecurity

- v `Certification Authority` v levem drevesu desen klik na `Certificate Templates` > `New` > `Certificate Template to Issue`
    - izberi `Key Recovery Agent`

- v `run` box (`win + R`) zaženi `certmgr.msc`
    - v levem drevesu desen klik na `Certificates - Current User/Personal` > `All Tasks` > `Request New Certificate...`
    - 2x klikni `Next`
    - označi checkbox za `Key Recovery Agent` in klikni `Enroll` in nato `Finish`

- v `Certification Authority` pod `Pending Requests` issuaj certifikat (desen klik nanj > `All tasks` > `Issue`)

- V `Certification Authority` desen klik na ime serverja > `Properties`
    - pod tabom `Recovery Agents` izberi `Archive the key`, pusti 1 agent in klikni `Add` ter nato `Ok` da dodaš prej issuan Key Recovery Agent certifikat



