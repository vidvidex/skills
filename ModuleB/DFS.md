# DFS

- https://www.vembu.com/blog/distributed-file-system-dfs-windows-server-2016-brief-overview/

## Install

- pod `Server Roles` izberi `File and Storage Services` > `File and iSCI` > `DFS Namespaces` in `DFS Replication`


## Naredi nov namespace

- v Server Managerju pod tools odpri `DFS Management`

- v levem meniju desen klik na `Namespaces` > `New namespace`
    - izberi server na katerem bo namespace hostan
    - izberi ime in klikni `Edit settings`
        - po potrebi spremeni path in permissione za celoten namespace (custom: Full permissions za `Administrators` in read only za `Domain Users`)
        ![](img/2021-09-06-12-28-43.png)
    - kot namespace type pusti Domain-based
- ko imaš izbran namespace dodaj še druge serverje na katerih bo DFS
    ![](img/2021-09-06-12-26-33.png)
    - wizard za dodajanje serverja je zelo podoben tistemu za dodajanje prvega (izbereš server, path za folder in permissione)

## Permissions

- želimo da ima lokalni administrator full permissions, ostali userji pa samo read. Poleg tega ima uporabnik `Roadw_Skill39` full permissions za folder `Roadw_Data`

- na nivoji DFS damo custom premissione:
    - full permissions za `Administrator`
    - read in write za `Domain Users`
- na nivoju NTFS damo enake permissione + po potrebi dodatne omejitve na nivoju folderja:
    - npr. v file explorerju desen klik na folder od `Roadw_Skill39` (`Roadw_Data`) > `Security` > `Edit`
        - dodaj uporabnika `Roadw_Skill39` in mu daj full permissions

- efektivne permissione lahko vidiš tako da izbereš nek folder, desen klik > `Properties` > `Security` > `Advanced` > `Effective Access` in izbereš nekega uporabnika
    ![](img/2021-09-16-14-45-06.png)
    - tu lahko npr. vidiš če ti dostop do folderja blokira DFS ali NTFS permission

## Replication

- v DFS Management v levem meniju desen klik na `Replicaton` > `New Replication Group`
    - pusti Multipurpose replication group
    - določi ime za replication group
    - dodaj serverje na katere bomo repliciral (v našem primeru DCje)
    - izberi full mesh topology
    - pusti Replicate continously in Full bandwidth
    - izberi Primary memberja (server, ki je avtoritativen ko se folder prvič replicira)
    - na primary memberju izberi mape, ki se replicirajo
    - na ostalih serverjih določi lokacijo replicirane mape
