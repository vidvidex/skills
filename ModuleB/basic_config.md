# Basic config


## Imena interfacov:

- `<lokacija> LAN` za notranja, npr `LINZ LAN`
- `<lokacija> WAN` za zunanja, npr `LINZ WAN`

## Dodajanje novega diska če v partition managerju napiše "The disk is offline because of policy set by an administrator" 

- v `Disk Management` desen klik na ime diska (npr. `Disk 1`) spodaj, levo od tistih modrih črt za particije. Prvič ga s klikom na `Online` daš online, nato pa še enkrat desen klik in ga inicializiraš (**GPT** ali MBR). 
- šele ko je inicializiran lahko narediš `New Simple Volume` (`D` je najbrž že zaseden zato najprej CD-ROMu spremeni črko)

## Windows Server 2019 core

- https://devtutorial.io/how-to-set-a-static-ip-address-on-windows-server-2019.html

- nastavljanje IPja, imena, domene:
    ```bash
    sconfig.exe
    ```
- v `sconfig.exe` nastaviš IP, DNS, ime ter ga dodaš v domeno, naprej pa ga konfiguriraš prek enega izmed serverjev z GUIjem (`ServerManager` > `Manage` > `Add servers`)


## NTP

- https://www.em-soft.si/myblog/elvis/?p=57

- `w32tm /config /manualpeerlist:NASLOV_NTP_SERVERJA /syncfromflags:MANUAL /reliable:yes`

- po tem bi bilo potrebno z ukazi updatat in restartat time service, lahko pa samo restartamo server

- tako konfiguriramo le glavni DC (`WINSRV4`) in naprave, ki niso v domeni. Tiste, ki so v domeni samodejno dobijo čas od DCjev

## Random notes

- ICMP je po defaultu izklopljen
