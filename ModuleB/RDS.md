# Remote Desktop Services

## Basic install 

- prek `Add Servers` dialoga iz enega serverja dodaj še ostale (lažji deployment kasneje)
- v `Add Roles and Features` pri Installation type izberi `Remote Desktop Services installation`
- pusti `Standard deployment` (quick bo vse naložil na isti server)
- izberi `Session-based desktop deployment`
- izberi na kateri server se bo naložil Connection Broker
- izberi na kateri server se bo naložil Web Access server
- izberi na kateri server se bo naložil Session Host
- označi, da dovoliš da se serverji med installanjem restartajo in klikni `Deploy`

## RD Gateway in RD Licensing

- v `Server Manager` pod `Remote Desktop Services` klikni na + za `RD Gateway`
    - gateway je lahko na istem serverju kot `Web Access` (WINSRV11)
    - izberi na kateri server naj se naloži
    - napiši FQDN
    - ko se naloži in preden klikneš `Close` klikni `Configure certificate`
        ![](img/2021-09-13-16-37-16.png)
        - klikni `Create new certificate` medtem ko imaš označen `RD Connetion Broker - Enable Single Sign On`
            - za ime napiši FQDN (`rds.skill39.at`)
            - označi `Store this certificate` in izberi folder kjer ga boš hitro našel
            - označi `Allow the certificate to be added to the Trusted Root Certification Authorities certificate store on the destination computers`
        - za ostale (`RD Connection broker - Publishing`, `RD Web Access` in `RD Gateway`) klikni na `Select existing certificate` in izberi prej narejen self-signed certifikat
            - označi `Allow the certificate to be added to the Trusted Root Certification Authorities certificate store on the destination computers`
        - po tem ko dodaš certifikat za vsako storitev moraš klikniti `Apply`
        - ko si 4x dodal certifikat levo klikni `RD Licensing`
            - izberi `Per User`
            - izberi license server
- klikni še na + za `RD Licensing`
    - izberi na kateri server naj se naloži
    - če prej nisi lahko tudi tu po tem ko se naloži spreminjaš nastavitve za licensing


- RDS je na `https://rds.skill39.at/RDWeb`
    - ne pozabi port forwarding in A record


## Remote Desktop prek RDS

- v `Server Manager` > `Remote Desktop Services` > `Collections` > `TASKS` klikni na `Create Session Collection`
    ![](img/2021-09-16-14-29-29.png)

    - izberi neko ime, npr. "Skills"
    - izberi Session host server, na katerem bo ta collection (ker imaš samo 1 session host izbire pravzaprav nimaš)
    - določi kdo ima dostop do tega collectiona (le `intra\Roadw_Skill39`)
    - ne enablaj user profile diska

- na clientu v `RD Web Access` se prijavi z `intra\Skill39_Roadw` 
    - če zaradi self signed certifikata ne dovoli prijave shrani ta certifikat na disk in ga importaj v `Trusted Root Certification Authorities`
- pod `RemoteApp and Desktops` se pojavi ikona za RDP
    ![](img/2021-09-16-14-37-29.png)
    - s klikom nanjo se z RDPjem povežeš na Session Broker (WINSRV7), ta pa preusmeri na Session Host (WINSRV6). Zato bo na vrhu zaslona pisalo da si z RDPjem povezan na WINSRV7 čeprav boš browsal po WINSRV6
    - ko si povezan na Session Host lahko tam dostopaš do uporabnikove mape `Roadw_Skill39` (glej da so permissioni pravilno nastavljeni)

## Dodaj link za reset passworda

- na serverju, ki hosta website za RDS (WINSRV11) v `ISS Manager` v levem meniju razširi `Sites / Defaul Web Site / RDWeb / Pages` in tu klikni na `Application Settings`
    - spremeni `PasswordChangeEnabled` na `true`

- v `C:\Windows\Web\RDWeb\Pages\en-US\login` najdi "autocomplete" in spodaj nekam zraven dopiši
    ```html
    <a href="password.aspx">Reset password</a>
    ```
- restartaj `ISS`

## Dodaj sliko na login page

- v `C:\Windows\Web\RDWeb\Pages\en-US\login` dodaj html za sliko

ALI

- v `C:\Windows\Web\RDWeb\Pages\Images` povozi eno izmed teh:
    - `mslogo_black.png` (mali logo spodaj desno)
    - `bg_globe_01.jpg` (ozadje)
    - ali kakšno drugo