# DNS

- https://computingforgeeks.com/install-and-configure-dns-server-in-windows-server/

- dodaj role `DNS Server`

## Foward lookup zone

- odpri `DNS Manager`
- na levi strani desen klik na ime serverja > `New zone...`
  - izberi `Primary zone`
  - izberi `Forward lookup zone`
  - napiši ime (npr. `example.net`)
  - pusti privzeto ime datoteke
  - po potrebi dovoli dynamic updates

## Reverse lookup zone


- odpri `DNS Manager`
- na levi strani desen klik na ime serverja > `New zone...`
  - izberi `Primary zone`
  - izberi `Reverse lookup zone`
  - izberi če delaš reverse lookup zone za IPv4 ali IPv6
  - napiši network ID da generira pravo ime
  - pusti privzeto ime datoteke
  - po potrebi dovoli dynamic update

## Dodaj nov record

- v `DNS Managerju` v levem drevesu desen klik na nek forward zone > `New Host (A or AAAA)...` (ali nek drug tip če potrebuješ nekaj drugega)
  - napiši ime in IP naslov
  - označi `Create associated pointer (PTR) record`
  - testiraj (npr. `ping`, `nslookup`)