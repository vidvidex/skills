# Site-to-site VPN

- https://www.youtube.com/watch?v=eJH56qMNRu8

- https://www.youtube.com/watch?v=xFYLIjv5EKI

- (https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-r2-and-2008/cc753178(v=ws.10))

- ni pravi site-to-site VPN ampak sta 2 remote-access VPNja, ki se skupaj obnašata kot site-to-site VPN

- naloži `Remote access` role, pod `Role services` izberi `DirectAccess and VPN (RAS)` in `Routing`

- ko konča nalaganje klikni na `Open the Getting Started Wizard`
    - v oknu ki se odpre klikni na `Deploy VPN only`

- ko se odpre `Routing and Remote Access` window:
    - desni klik na ime serverja v levem meniju > `Configure and Enable Routing and Remote Access`
        - izberi `Custom configuration` in nato `VPN access`, `Demand-dial connections (used for branch office routing)` in `NAT`
        - ko vpraša za startanje servica klikni `Start service`

    - desni klik na ime serverja v levem meniju > `Properties`
        - v `Security` tabu označi `Allow custom IPsec policy for L2TP/IKEv2 connection` in napiši pre-shared key
        - ~~v `IPv4` tabu označi `Static address pool` in dodaj pool IP naslovov, ki se bodo uporabljali za povezavo. Uporabi pool iz lokalnega omrežja tega serverja. Za samo 1 site-to-site lahko določiš samo 2 naslova, za mesh pa več~~ **DHCP je ok**
    
    - klikni na `Network interfaces` v levem meniju > `New Demand-dial Interface`
        - določi ime interfaca: naj bo ime serverja s katerim se povezuješ
        - pusti `Connect using VPN`
        - izberi `L2TP`
        - napiši IP serverja s katerim se povezuješ
        - označi oba checkboxa (`Route IP packets on this interface` in `Add a user account so a remote router can dial in`)
        - dodaj statično routo do remote networka, metric daj na 1
        - določi geslo s katerim se bo remote server povezal na ta server (username je že določeno - enako kot interface name)
        - določi username in password s katerim se bo ta server povezal na remote server:
            - username: ime tega serverja
            - domeno pusti prazno
            - password tak kot si ga nastavil na drugi strani (oziroma najbrž kar povsod)

    - ko je interface narejen, desen klik nanj > `Properties`
        - v `Options` tabu označi `Persistent connection` (da se postavi po rebootu)
        - v `Security` tabu klikni `Advanced settings` in tam napiši pre-shared key

    - desen klik na ime serverja v levem meniju > `All tasks` > `Restart`

    - desen klik na interface > `Connect`