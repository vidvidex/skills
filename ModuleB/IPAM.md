# IPAM

- ne sme biti na DC-ju

- https://msftwebcast.com/2020/01/install-and-configure-ipam-in-windows-server-2019.html

- https://newhelptech.wordpress.com/2018/12/02/implementing-configuring-ipam-in-windows-server-2016/

- naloži feature (ne role) `IP Address Management (IPAM) Server`

- v server managerju levo klikni `IPAM`

- klikni `Provision the IPAM Server`
    - pusti default database
    - napiši GPO name prefix
    - klikaj next do konca

- klikni `Configure server discovery`
    - klikni `Get Forests`
    - medtem ko išče zapri ta Configure server discovery windows in ga še enkrat odpri ko dobiš notification da je iskanje končano
    - dodaj domeno (izberi v spodnjem dropdownu)
    - poskrbi da ima označene DC, DHCP server in DNS server (default)

- klikni `Start server discovery`
    - počakaj da se discovery konča. Ta čas lahko v PowerShellu kot admin poženeš `Invoke-IpamGpoProvisioning –Domain intra.skill39.at –GpoPrefixName Skill39-IPAM -DelegatedGpoUser Administrator@intra.skill39.at -IpamServerFqdn WINSRV6.intra.skill39.at` (ime je napisano večkrat v installation wizardih, za Domain in GpoPrefixName bi vprašal tudi brez, potreben pa je tudi parameter -DelegatedGpoUser; na srečo lahko pri PowerShellu napišeš `-` in nato klikaš `tab` dokler ni autofillano pravo ime parametra)
        - na vprašanja odgovori z Yes (default) (4x)
        - na DC-ju updataj GPO policyje: `gpupdate /force`

- ko je Server discovery končan klikni na `Select or add servers to manage and verify IPAM access`
    - desen klik na DCja (WINSRV4 in WINSRV6) server > `Edit server` > če ni obkljukaj DHCP in nastavi Managebility status na "Managed"
    - ko klikneš ok izpiše napako da ne najde GPOja, klikni ok in nadaljuj, očitno še vedno dela
    - desen klik na enega izmed serverjev > `Retrieve All Server Data`

- v levem meniju izberi `DNS and DHCP Servers`
    - desni klik na enega izmed DHCP serverjev > `Create DHCP Scope` > konfiguriraj scope 
        - ime
        - start in end IP
        - maska
        - default gateway (pod options izberi nastavitev `Router`)
        - DNS serverji (pod options nastavitev `DNS Server`)

- levem meniju izberi `DHCP Scopes`
    - desen klik na scope > `Configure DHCP Failover`
        - za partnerja izberi drugi DC
        - določi ime in secret
        - če je potrebno spremeni Advanced properties
    
- Testiranje:
    - na nekem clientu poženi `ipconfig /renew` da zahtevaš nov IP
    - na IPAM serverju desen klik na enega izmed serverjev > `Retrive All Server Data`, ko konča v levem meniju klikni na `IP Address Blocks` in refreshaj page (tist gumb zgoraj desno zraven tistega kjer dodajaš Feature), pod Utilized Addresses bi nekje moralo pisati "1"


## DHCP Relay

- *namesto tega bi lahko samo postavil DHCP server nekje v tistem subnetu, npr. na "routerju"*

- poskrbi da obstaja pool

- na remote VPN serverjih (tistih, ki so v subnetu kjer hočemo enablat DHCP) odpri `Routing and Remote access`

- v levem meniju najdi `DHCP Relay Agent` > desen klik > `Properties` > dodaj IP DHCP serverja

- še enkrat desen klik na `DHCP Relay Agent` > `New Interface` > izberi LAN interface in pusti ostalo na default
