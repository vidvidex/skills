# Notes



## WINSRV1

- ping rule že obstaja, samo dovoliš ga
- dovolit je treba inbound NTP (UDP 123)
- pri dodajanju novega diska desen klik na disk (2x, online + initialize)
- update SOA serijsko na trenuten datum


## WINSRV2,3,12

- morajo biti sami sebi default gateway (lokalen .254 naslov)
- pri VPN disable CHAP
- pri site2site VPN ni potrebno dajati statičnega IP poola (DHCP je ok)


## WINSRV4

- za DNS nastavi 10.1.0.1 namesto 127.0.0.1
- DFS:
    - disable inheritance
    - zamenjaj "users" z "domain users"
    - advanced sharing, daš administrators full permissions in read za users
    - custom permissions, spet administrator full permissions, domain users pa read only
    - NTFS permissione je potrebno nastaviti na roke na obeh serverjih (mogoče bo na obeh hkrati če delaš na `\\intra.skill39.at\nekineki`, za vsak slučaj preveri)
    - `win;win` za dodajanje dveh serverjev hkrati
- GPO:
    - `Group Policy Results` da vidiš kaj se bo kaj applyal na računalnik (user se mora najprej vsaj 1x loginat)

## WINSRV5

- DNS serverja daš najprej 10.1.0.2, nato pa še 10.1.0.1 (najprej sebe)


## WINSRV6

- IPAM:
    - dodaj "domain computers" group na vse 3 GPOje (edit, desen klik na ime policyja levo zgoraj) ???

- pri dodajanju novih userjev bi lahko takoj specifical še home drive (potreben bo še Set-ADUser) `homeDrive` in `homeDirectory`




# Teams meeting 

- včasih pomaga če disablaš IPv6
- samo `intra` za ime domene včasih ne dela, potreben `intra.skill39.at`
- pri installanju rolov dodaj še telnet (lažji debugging)


## RDP

- vse serverje daš v Add Servers
- pri features daš standard deployment, session based, izbereš na katerem serverju je kak feature (samo 3 so obvezni, ostalo je vse optional (npr. licensing))
- naknadno dodaš licensing (klikneš na ikonco na diagramu)
- gateway na istem serverju kot web access (oba delata na 443)
- pri koncu wizarda vpraša za konfiguracijo certifikatov
- kljukica za trusted root
- vsak certifikat posebaj, klikni apply po vsakem
- certifikat narediš iz IIS (na winsrv11) ali prek mmc tako kot za key recovery agent

- so self signed certifikati ok za RDS? Forum

- licensing per user

- logon method password ali pusti da user izbere

- za fqdn potreben port forward
    - na NAT WAN interface properties, zgoraj Services and Ports

## CA

- cert se da requestat prek web ui