# Users

## Home drive na shared folderju

- v `Active Directory Users and Computers` desen klik na uporabnika (lahko tudi izbereš več uporabnikov in to narediš za vse hkrati) > `Properties` > `Profile`
    - pod `Home folder` označi `Connect` in izberi črko ter napiši path do network shara, `%username%` se avtomatsko zamenja z uporabnikovim imenom (npr. `\\intra.skill39.at\Skill39-Space\Skill39-Profiles\%username%`)

    - to dela tudi v `Active directory Administrative Center` kjer v user properties lahko nastaviš home folder ampak ta ne bo sam naredi tega folderja, medtem ko `Active Directory Users and Computers` bo.

## Importanje userjev iz CSVja v AD

- https://docs.microsoft.com/en-us/powershell/module/activedirectory/new-aduser?view=windowsserver2019-ps

```powershell
$users = Import-Csv C:\users.csv

$homeDrive = "X:"

foreach ($user in $users) {

    $homeDirectory = "\\intra\Skill39-space\Skill39-Profiles\" + $user.Username

    New-ADUser -SamAccountName $user.Username -DisplayName $user.Name -Path $user.OU -AccountPassword (ConvertTo-secureString $user.Password -AsPlainText -Force)
    Set-ADUser -Identity $user.Username -HomeDirectory $user.HomeDirectory -HomeDrive $user.HomeDrive
}
```
- `HomeDrive` mora biti oblike `X:` (ena velika črka in dvopičje)
- parametri so listani v PowerShell ISE na desni
- poleg tistih v skriptu zgoraj jih je na voljo še cel kup
- accounti so po defaultu disablani, če se spomniš lahko dodaš flag `Enabled $True` ali pa samo v `AD Administrative Centre` oz `AD Users and Computers` izbereš vse in jih enablaš