# VMware ESXi

## Networking

- poseben vSwitch za vsako nalogo, tako da je okolje povsem ločeno od zunaj (da se ne zgodi da kak zunanji DHCP deli naslove kjer nebi smel)
- vsako omrežje v nalogi svoj portgroup z drugačnim VLANom (tudi če naloga ne zahteva nobenih vlanov), da so omrežja ločena (ker so povezana na isti vSwitch in bo npr DHCP drugače spet delal težave). Vsamemu VMu nato daš network interface v pravi portgroup.

## Ustvarjanje VMov

- če so vsi enaki narediš enega (ga poimenuješ npr. `template`) in nato uporabiš `clone-vms.sh` (in ga pred tem seveda prilagodiš) za kloniranje VMov (pri Windowsih imajo enak SID tako da ga je treba spremeniti na roke)
- ko imaš narejene VMe jih spraviš v začetno stanje (npr. naložiš package ki bodo naloženi na tekmovanju, spremeniš hostname, dodaš dodatne network interface, ...), in nato narediš snapshot vsakega VMa, da se lahko kasneje vrneš na začetek

## Remote access

- za delo od doma in podobno je dobro na enem izmed virtualizatorjev narediti remote access VPN server, preko katerega se nato povežeš na ESXi, CML in podobno.
- slabša rešitev je port forwarding. Sicer dela a je potrebno konstantno konfigurirati nove porte ko potrebuješ dodaten service
