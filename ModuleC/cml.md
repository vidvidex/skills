# CML

## Breakout tool
- `Tools` > `Breakout tool`
- poženi z `breakout-neki-neki.neki ui`
- na `localhost:8080` greš pod `Configuration`
    - Controller address: `https://cml.glab.local` (url od CMLja)
    - izklopi "Verify TLS Certificate"
    - napiši svoje username in password
- pod `Labs` refreshaj labe in za željeni lab spremeni status na On


## Shranjevanje konfiguracije

- `copy run start`
- pod "Edit configs" > "Fetch from device"


## Default credentials

- https://developer.cisco.com/docs/modeling-labs/#!faq/reference-platform-and-images-questions
- za alpine je `cisco:cisco`

