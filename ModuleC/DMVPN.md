# DMVPN

- https://networklessons.com/cisco/ccie-enterprise-infrastructure/dmvpn-dual-hub-single-cloud

- https://networklessons.com/cisco/ccie-routing-switching/dmvpn-over-ipsec

- https://www.cisco.com/c/en/us/support/docs/security-vpn/ipsec-negotiation-ike-protocols/29240-dcmvpn.html

- https://www.cisco.com/c/en/us/support/docs/security-vpn/ipsec-negotiation-ike-protocols/81552-iosca.html

- https://content.cisco.com/chapter.sjs?uri=/searchable/chapter/content/en/us/td/docs/switches/lan/catalyst9600/software/release/16-12/configuration_guide/ip/b_1612_ip_9600_cg/configuring_nsrp.html.xml&platform=Cisco%20Catalyst%209600%20Series%20Switches

## Hub

```txt
crypto ipsec transform-set TRANSFOR_SET esp-aes 256 esp-sha512-hmac

crypto ipsec profile VPN_PROFILE
    set security-association lifetime seconds 120
    set transform-set TRANSFOR_SET 

int tunnel 0
    ip addr IP_V_VPN_OMREŽJU MASKA_VPN_OMREŽJA
    ip nhrp authentication Passw0rd
    ip nhrp map multicast dynamic
    ip nhrp network-id 1
    no ip split-horizon eigrp 65001     ! če uporabljaš eigrp, 65001 je ASN
    no ip next-hop-self eigrp 65001
    tunnel source ZUNANJI_FIZIČNI_IP_ROUTERJA
    tunnel mode gre multipoint
    tunnel key 0
    tunnel protection ipsec profile VPN_PROFILE
```

za dodatne hube dodaj še
```txt
    ip nhrp map MAIN_HUB_VIRTUAL_IP MAIN_HUB_PHYSICAL_IP
    ip nhrp map multicast MAIN_HUB_PHYSICAL_IP
    ip nhrp nhs MAIN_HUB_VIRTUAL_IP
```

## Spoke

```txt
crypto ipsec transform-set TRANSFOR_SET esp-aes 256 esp-sha512-hmac

crypto ipsec profile VPN_PROFILE
    set security-association lifetime seconds 120
    set transform-set TRANSFOR_SET

int tunnel 0
    ip addr IP_V_VPN_OMREŽJU MASKA_VPN_OMREŽJA
    no ip redirects
    ip mtu 1440
    ip nhrp authentication Passw0rd
    ip nhrp map multicast dynamic

    ! Za vsak hub router potrebuješ te 3 vrstice
    ip nhrp map HUB_1_VIRTUAL_IP HUB_1_PHYSICAL_IP
    ip nhrp map multicast HUB_1_PHYSICAL_IP
    ip nhrp nhs HUB_1_VIRTUAL_IP
    
    ip nhrp map HUB_2_VIRTUAL_IP HUB_2_PHYSICAL_IP
    ip nhrp map multicast HUB_2_PHYSICAL_IP
    ip nhrp nhs HUB_2_VIRTUAL_IP

    ip nhrp network-id 1
    tunnel source ZUNANJI_FIZIČNI_IP_ROUTERJA
    tunnel mode gre multipoint
    tunnel key 0
    tunnel protection ipsec profile VPN_PROFILE
```
