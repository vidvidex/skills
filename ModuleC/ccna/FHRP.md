# First Hop Redundancy Protocols

- default prioriteta: 100
  
- router z najvišjo prioriteto je active

- preempt: router avtomatsko postane active

## HSRP

```txt
R1#(config) int fa0/0
R1#(config-if)ip add 10.1.1.1 255.255.255.0

R1#(config-if) standby 1 ip 10.1.1.100      # Virtual IP
R1#(config-if) standby 1 name HSRP_TEST
R1#(config-if) standby 1 priority 110       # Ta bo active
R1#(config-if) standby 1 preempt
```

```txt
R2#(config) int fa0/0
R2#(config-if)ip address 10.1.1.2 255.255.255.0

R2#(config) int fa0/0
R2#(config-if) standby 1 ip 10.1.1.100      # Virtual IP
R2#(config-if) standby 1 name HSRP_TEST
R2#(config-if) standby 1 priority 100
R2#(config-if) standby 1 preempt
```

## VRRP

```txt
R1# int fa0/0
R1# ip add 10.1.1.1 255.255.255.0

R1# int fa0/0
R1# vrrp 10 ip 10.1.1.100                   # Virtual IP
R1# vrrp 10 name VRRP_TEST
R1# vrrp 10 priority 110                    # Ta bo active
```

```txt
R2# int fa0/0
R2# ip address 10.1.1.2 255.255.255.0

R2# int fa0/0
R2# vrrp 10 ip 10.1.1.100                   # Virtual IP
R2# vrrp 10 name VRRP_TEST
R2# vrrp 10 priority 100
```
