# Access Control Lists

## Standard

- samo source IPv4
- kar se da blizu destinacije
- 1-99, 1300-1999

```txt
R1(config)# access-list 10 permit 192.168.10.0 0.0.0.255
```

## Extended

- kar se da blizu sourca
- 100-199, 2000-2699

```txt
R1(config)# access-list 100 permit tcp 192.168.10.0 0.0.0.255 any eq www
```

## Named

- preferred method
- dela za standard in extended

```txt
R1(config)# ip access-list extended FTP-FILTER
R1(config-ext-nacl)# permit tcp 192.168.10.0 0.0.0.255 any eq ftp
R1(config-ext-nacl)# permit tcp 192.168.10.0 0.0.0.255 any eq ftp-data
```


## Apply to interface

```txt
R1(config-if) # ip access-group {access-list-number | access-list-name} {in | out}
```
