# EtherChannel

- vsi interfaci morajo imeti enako hitrost, enak duplex, enake VLANe


## LACP

```txt
S1(config)# interface range f0/1-2
S1(config-if-range)# channel-group 1 mode active

S1(config)# interface port-channel 1
S1(config)# switchport mode trunk
S1(config)# switchport trunk allowed vlan 1,2,20
```
