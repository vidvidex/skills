# Routing

## Static

  
```sh
ip route <network> <mask> { next-hop | exit-interface }
```
default route:
```sh
ip route 0.0.0.0 0.0.0.0 { next-hop | exit-interface }
ipv6 route ::/0 { next-hop | exit-interface }
```


## OSPFv2

```sh
router ospf <process num>
    network <network> <wildcard> area 0
```

## EIGRP

```sh
router eigrp <ASN>
    network <network> <wildcard>
```

vsi routerji morajo imeti enak ASN
