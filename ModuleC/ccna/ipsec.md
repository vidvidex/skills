# Cisco site to site IPSec VPN

Avtentikacija pre-shared key ali certifikat

Izvedba s crypto map ali s tuneli

## Pre-shared key

    crypto isakmp policy 10
    encr aes
    authentication pre-share
    group 5
    lifetime 3600

    crypto isakmp key PASSWORD address IP_DRUGEGA_ROUTERJA

## Certifikat

pazi da je ura pravilno nastavljena (`ntp` ali `clock set`)


### Cisco CA

#### Server

```text
# Generate key
crypto key generate rsa general-keys label NAME modulus 4096 exportable

# Export key to nvram
crypto key export rsa NAME pem url nvram: 3des PASSWORD

ip http server

# Naredi CA server (NAME je enak kot pri prvi komandi)
crypto pki server NAME
 database url nvram:
 database level minimum
 issuer-name CN=FQDN_TEGA_ROUTERJA
 grant auto
 lifetime ca-certificate 365
 lifetime certificate 200
 lifetime crl 24
 no shut
```

#### Client

nastavi domain in hostname

```text
crypto key generate rsa modulus 2048

crypto pki trustpoint TRUSTPOINT_NAME
 enrollment retry count 5
 enrollment retry period 3
 enrollment url http://IP_CA_SERVERJA
 revocation-check none

crypto pki authenticate TRUSTPOINT_NAME

crypto pki enroll TRUSTPOINT_NAME
```

### Linux CA

```text
crypto isakmp policy 10
 encr aes
 authentication rsa-sig
 group 5
 lifetime 3600

crypto pki trustpoint CA_SERVER_NAME
 enrollment terminal
 revocation-check none

crypto pki authenticate CA_SERVER_NAME
 <prilepi crt od ca-ja>

crypto pki enroll CA_SERVER_NAME
 <no,no,yes; kopiraj csr na ca in ga podpiši>
```

podpis ključa na CA:

```sh
openssl x509 -req -days 1000 -in client.csr -CA ca.crt -CAkey ca.key -out client.crt -CAcreateserial
```

nato spet na routerju:

    crypto pki import CA_SERVER_NAME certificate
     <prilepi podpisan crt file za ta router>

## S crypto map

    crypto ipsec transform-set TRANS_SET esp-aes 256 esp-sha512-hmac

    # ACL ki pove iz kje kam gre promet preko vpnja
    ip access-list extended VPN_ACL
     permit ip SRC_NET SRC_MASK DEST_NET DEST_MASK

    crypto map CRYPTO_MAP 10 ipsec-isakmp
     set peer IP_DRUGEGA_ROUTERJA
     set security-association lifetime seconds 900
     set transform-set TRANS_SET
     match address VPN_ACL

    interface GigabitEthernet0/0
     crypto map CRYPTO_MAP  # Dodaš map na zunanji interface

    # Potreben tudi routing (OSPF/EIGRP/static)

## S tuneli

https://networklessons.com/cisco/ccie-routing-switching-written/ipsec-static-virtual-tunnel-interface

```text
crypto ipsec transform-set TRANS_SET esp-aes 256 esp-sha-hmac

crypto ipsec profile IPSEC_PROFILE
 set transform-set TRANS_SET

interface Tunnel0
 ip address IP_TUNELA MASK
 tunnel source LOCAL_IP
 tunnel destination IP_DRUGEGA_ROUTERJA
 tunnel mode ipsec ipv4
 tunnel protection ipsec profile IPSEC_PROFILE

 # Potreben routing preko tunela
```

## Verify

```text
show crypto isakmp sa [detail]
show crypto ipsec sa
```
