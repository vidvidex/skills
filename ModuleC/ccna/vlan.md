# VLAN

## Create

```txt
S1# configure terminal
S1(config)# vlan 20
S1(config-vlan)# name sales
```

## Access

```txt
S1# configure terminal
S1(config)# interface fa0/1
S1(config-if)# switchport mode access
S1(config-if)# switchport access vlan 20
```

## Trunk

```txt
S1(config)# interface fa0/0
S1(config-if)# switchport mode trunk
S1(config-if)# switchport trunk native vlan 99
S1(config-if)# switchport trunk allowed vlan 10,20,30,99
```


## Router-on-a-stick

```txt
R1(config)# interface g0/0.20
R1(config-subif)# encapsulation dot1Q 20
R1(config-subif)# ip add 192.168.20.1 255.255.255.0
```
