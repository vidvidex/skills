# NAT

```txt
show ip nat translations
show ip nat statistics
```


## Static NAT

- nastavi map med privatnim in javnim naslovom (inside local ter inside global)
    ```txt
    R1(config)# ip nat inside source static 192.168.10.254 209.165.201.5
    ```

- nastavi inside in outside interface
    ```txt
    R1(config)# interface serial 0/1/0
    R1(config-if)# ip address 192.168.1.2 255.255.255.252
    R1(config-if)# ip nat inside

    R1(config)# interface serial 0/1/1
    R1(config-if)# ip address 209.165.200.1 255.255.255.252
    R1(config-if)# ip nat outside
    ```

## Dynamic NAT

- definiraj pool naslovov
    ```txt
    R1(config)# ip nat pool NAT-POOL 209.165.200.226 209.165.200.240 netmask 255.255.255.224
    ```

- naredi ACL ki definira privatne naslove, ki bodo prevedeni
    ```txt
    R1(config)# access-list 1 permit 192.168.0.0 0.0.255.255
    ```

- poveži pool in ACL (poveže ACL `1` in pool `NAT-POOL`)
    ```txt
    R1(config)# ip nat inside source list 1 pool NAT-POOL
    ```

- nastavi inside in outside interface
    ```txt
    R1(config)# interface serial 0/1/0
    R1(config-if)# ip nat inside

    R1(config)# interface serial 0/1/1
    R1(config-if)# ip nat outside
    ```


## PAT (NAT overload)

- z enim javnim naslovom:
    ```txt
    R1(config)# ip nat inside source list 1 interface serial 0/1/1 overload
    R1(config)# access-list 1 permit 192.168.0.0 0.0.255.255
    R1(config)# interface serial0/1/0
    R1(config-if)# ip nat inside

    R1(config)# interface Serial0/1/1
    R1(config-if)# ip nat outside
    ```

- s poolom javnih naslovov:
    ```txt
    R1(config)# ip nat pool NAT-POOL 209.165.200.226 209.165.200.240 netmask 255.255.255.224
    R1(config)# access-list 1 permit 192.168.0.0 0.0.255.255
    R1(config)# ip nat inside source list 1 pool NAT-POOL overload

    R1(config)# interface serial0/1/0
    R1(config-if)# ip nat inside
    R1(config-if)# exit
    R1(config)# interface serial0/1/1
    R1(config-if)# ip nat outside
    ```
