# Security

## Izklopi neuporabljene porte

```txt
S1(config)# interface range fa0/8 - 24
S1(config-if-range)# shutdown
```

## Port security

- v CCNA samo za access porte, trunk so out of scope

```txt
show port-security interface
show port-security address
```

```txt
S1(config)# interface f0/1
S1(config-if)# switchport mode access
S1(config-if)# switchport port-security
```

- nastavi max št MAC naslovov na portu (default: 1)
    ```txt
    S1(config-if)# switchport port-security maximum <value> 
    ```

- ročno nastavi MAC
    ```txt
    S1(config-if)# switchport port-security mac-address <mac-address>
    ```

- dinamično nastavi
  - po defaultu je takoj ko prižgeš port security trenutno povezana naprava whitelistana, a to ni dodano v startup config
  - da se naučen MAC ohrani:
    ```txt
    S1(config-if)# switchport port-security mac-address sticky 
    ```

## DHCP snooping

```txt
S1(config)# ip dhcp snooping
S1(config)# interface f0/1                  # Na f0/1 je naš DHCP server, zaupamo portu
S1(config-if)# ip dhcp snooping trust

S1(config)# interface range f0/5 - 24       # Na teh portih so naprave, ki jim ne zaupamo, damo limit 6 packetov/sec za vlane 5,10,50-52
S1(config-if-range)# ip dhcp snooping limit rate 6
S1(config-if-range)# exit
S1(config)# ip dhcp snooping vlan 5,10,50-52
```


## Portfast

- samo za porte proti end devicom (če je proti drugemu switchu lahko nastane loop)

- preskoči listening in learning faze in takoj skoči iz blocking v forwarding

- enable na enem interfacu
    ```txt
    S1(config)# interface fa0/1
    S1(config-if)# switchport mode access
    S1(config-if)# spanning-tree portfast
    ```

- enable na vseh interfacih
    ```txt
    S1(config)# spanning-tree portfast default
    ```

## BPDU Guard

- interface posluša za BPDUje tudi ko je PortFast vklopljen
  
- z BPDU Guardom bo interface postavljen v error-disabled če dobi BPDU medtem ko je PortFast vklopljen

- enable na enem interfacu
    ```txt
    S1(config)# interface fa0/1
    S1(config-if)# spanning-tree bpduguard enable
    ```

- enable na vseh interfacih
    ```txt
    S1(config)# spanning-tree portfast bpduguard default
    ```