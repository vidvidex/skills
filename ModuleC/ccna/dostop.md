# Zavarovanje dostopa


## Secure user exec

```txt
S1# configure terminal
S1(config)# line console 0          # Ponavadi imaš samo 1 console line
S1(config-line)# password <password>     
S1(config-line)# login              # Enable user exec access
S1(config-line)# end
S1#
```


## Secure privileged exec


```txt
S1# configure terminal
S1(config)# enable secret <password>
S1(config)# exit
S1#
```


## SSH

```txt
ip domain-name <domain>
ip ssh version 2

crypto key generate rsa

line vty 0 4
transport input ssh
login local

username <username> secret <password>
```


## Password encryption

```txt
S1# configure terminal
S1(config)# service password-encryption
S1(config)#
```
