# CA

## Server

```txt
crypto key generate rsa general-keys exportable label LABEL modulus 4096

! 3des je najboljša možnost (AES manjka)
crypto key export rsa LABEL pem url nvram: 3des PASSWORD

ip http server

crypto pki server IME_SERVERJA
    database url nvram:
    database level minimum
    issuer-name CN=FQDN_TEGA_SERVERJA
    grant auto
    lifetime ca-certificate 365
    lifetime certificate 200
    lifetime crl 24
    no shut ! Po no shut te vpraša za geslo
```

**Če je ta CA server hkrati tudi client (npr za VPN) mora imetu še dodaten trustpoint (glej konfiguracijo za client) z drugačnim imenom kot je glavni**

## Client

```txt
crypto key generate rsa modulus 4096

crypto pki trustpoint TRUSTPOINT_NAME
    enrollment retry count 5
    enrollment retry period 3
    enrollment url http://IP_CA_ROUTERJA
    revocation-check none

crypto pki authenticate TRUSTPOINT_NAME     ! yes

crypto pki enroll TRUSTPOINT_NAME           ! no, no, yes
```
