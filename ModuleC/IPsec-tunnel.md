# IPsec tunnel VPN

- https://networklessons.com/cisco/ccie-routing-switching-written/ipsec-static-virtual-tunnel-interface

- https://blog.apnic.net/2020/07/29/how-to-ipsec-vpn-configuration/

- problem: IPsec je unicas only, routing protokoli, ki se zanašajo na multicast (OSPF, EIGRP, ...) ne delajo s tem.

- avtentikacija:
    - pre-shared key, glej PSK.md
    - certifikati, glej CA.md

```txt
crypto ipsec transform-set VPN_TRANS esp-aes 256 esp-sha512-hmac

crypto ipsec profile VPN_PROFILE
    set transform-set VPN_TRANS

interface Tunnel0
    ip addr IP_TEGA_ROUTERJA_V_VPN_OMREŽJU MASKA_VPN_OMREŽJA
    tunnel source ZUNANJI_IP_TEGA_ROUTERJA
    tunnel destination ZUNANJI_IP_DEST_ROUTERJA
    tunnel mode ipsec ipv4
    tunnel protection ipsec profile VPN_PROFILE
```
