# Pre-shared keys

```txt
crypto isakmp policy 10
    encr aes
    hash sha512
    authentication pre-share
    group 5
    lifetime 3600

crypto isakmp key PASSWORD address IP_DRUGEGA_ROUTERJA
```
