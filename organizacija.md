# Organizacija

## Postopek izvedbe naloge

1. Ugotoviš kako se vsaka stvar naredi 
   - iščeš na internetu, sprašuješ ostale, ...
   - traja zelo dolgo
   - za vsako stvar si zapišeš točen postopek kako to ponovno postaviti 
     - katere package je pogrebno naložiti
     - katere file je treba spremeniti in kako
     - katere gumbe je potrebno klikniti
     - zapišeš kakšne pogoste težave/zanimive opazke
     - včasih si je najbolje kar izpisati celoten delujoč config file
     - pri Ciscotu si shraniš running config
     - pri Windowsih včasih shraniš kak screenshot, na keterem označiš gumb ki ga je treba klikniti
   - najdeš način, ki zahteva kar se da malo pisanja konfiguracije iz glave. Če se le da spremeniš obstoječi sample config, kakšne specifične ukaze prepišeš iz help/man paga, ...
   - zapišeš si vir (ponavadi link) kje si to dobil (mogoče je še najbolje uporabiti kak browser extension za to (akademiki imajo cel kup takih stvari, npr. https://www.zotero.org))

2. Restoraš snapshote in celotno nalogo ponoviš. Pri tem slediš zapiskom in ko prej ali slej ugotoviš da si si nekaj napisal narobe ali pozabil napisati še enkrat pogledaš vire ki si jih zbral ko si prvič ugotavljal kako to postaviti in popraviš zapiske tako da bodo pravilni.

3. Ko si relativno prepričan da so zapiski ok je čas da nalogo karseda velikokrat ponoviš in pri tem naletiš na čim več težav (napak pri konfiguraciji). Če nanje naletiš pri vaji boš na tekmovanju veliko hitreje odkril razlog in ga tako lažje popravil

## Beleženje časa

- nekam (papir/shared Google Docs/Excel/...) si vsak zapiše od kdaj in do kdaj je delal ter kaj je delal (če delata različne stvari/pogosto prideta ob različnih časih je najbolje da imata zapisano v ločenih stolpcih)