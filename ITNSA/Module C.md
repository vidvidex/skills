## BME280

- <https://projects.raspberrypi.org/en/projects/build-your-own-weather-station/2>

<https://pypi.org/project/RPi.bme280/>

<https://www.dummies.com/programming/python/what-is-i2c-python-programming-basics-for-the-raspberry-pi/>

```bash
sudo apt install python3-pip
pip3 install rpi.bme280
pip3 install smbus2
```

```python
import bme280
import smbus2
from time import sleep

port = 1
address = 0x76
bus = smbus2.SMBus(port)

bme280.load_calibration_params(bus,address)

while True:
    data = bme280.sample(bus,address)
    print(round(data.humidity,2), round(data.pressure,2), round(data.temperature,2))
    sleep(0.5)
```

## InfluxDB, Grafana

<https://www.influxdata.com/>
<https://grafana.com/>

    wget https://dl.influxdata.com/influxdb/releases/influxdb2-2.0.4-amd64.deb
    sudo dpkg -i influxdb2-2.0.4-amd64.deb
    wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
    echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
    sudo apt-get install -y influxDB grafana
    sudo systemctl enable grafana-server.service
    sudo systemctl enable influxdb

## LED matrix

`sudo -H pip3 install --upgrade --ignore-installed pip setuptools`

<https://luma-led-matrix.readthedocs.io/en/latest/python-usage.html>

### Use spi1 instead of spi0

<https://raspberrypi.stackexchange.com/questions/73346/how-to-enable-spi1-and-spi0-at-the-same-time>

<https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md>

## API

<https://flask.palletsprojects.com/en/1.1.x/>

```bash
pip3 install Flask

export FLASK_ENV=development
export FLASK_APP=api.py
flask run --host=0.0.0.0
```

### Basic Auth

    pip3 install Flask-BasicAuth

## SSH portforwarding

ssh <root@ws.netlabs.xyz> -p 12022 -L 8080:80
<https://phoenixnap.com/kb/ssh-port-forwarding>

## Service

```
[Unit]
Description=Kiosk Launcher
After=systemd-user-sessions.service

[Service]
User=gasilec
ExecStart=/usr/bin/startx
Restart=always

[Install]
WantedBy=multi-user.target 
```