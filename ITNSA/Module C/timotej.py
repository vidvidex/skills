#! /usr/bin/python # coding: utf-8
import bme280script
import os
import sys import datetime
# import json
# pip install influxdb from influxdb
import InfluxDBClient
# influx configuration - edit these
ifuser = "grafana"
ifpass = "grafana"
ifdb = "home"
ifhost = "127.0.0.1"
ifport = 8086
measurement_name = "system"

# take a timestamp for this measurement time = datetime.datetime.utcnow()
# format the data as a single measurement for influx
# sudo ln -s remote_sensor remote_sensor.py

body = [
    {
        "measurement": measurement_name,
        "time": time,
        "fields": {
            "temp": bme280script.t,
            "humidity": bme280script.h,
            "pressure": bme280script.p
        }
    }
]

# connect to influx
ifclient = InfluxDBClient(ifhost, ifport, ifuser, ifpass, ifdb)

# write the measurement
ifclient.write_points(body)
