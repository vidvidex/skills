import bme280
import smbus2
from time import sleep
from influxdb import InfluxDBClient

from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.core.virtual import sevensegment
from luma.led_matrix.device import max7219

# MBE280 connection
port = 1
address = 0x76
bus = smbus2.SMBus(port)

bme280.load_calibration_params(bus, address)


# InfluxDB connection
client = InfluxDBClient('localhost', 8086, 'grafana', 'grafana', 'skills')
client.switch_database('skills')

# Change port to 1 to use spi1 instead of spi0
serial = spi(port=1, device=0, gpio=noop())
device = max7219(serial, cascaded=1)
seg = sevensegment(device)

while True:

    data = bme280.sample(bus, address)

    temperature = round(data.temperature, 2)
    humidity = round(data.humidity, 2)
    pressure = round(data.pressure, 2)

    line = f'test temperature={temperature},humidity={humidity},pressure={pressure}'
    client.write([line], {'db': 'skills'}, 204, 'line')

    seg.text = f'{temperature} °C'

    print(line)
    sleep(1)
