#!/usr/bin/python3

from luma.core.interface.serial import i2c, spi, pcf8574
from luma.core.interface.parallel import bitbang_6800
from luma.core.render import canvas
from luma.oled.device import ssd1306, ssd1309, ssd1325, ssd1331, sh1106, ws0010

from pathlib import Path
from PIL import Image

serial = i2c(port=1, address=0x3C)
device = ssd1306(serial)

img_path = '/opt/flags/slo.png'

logo = Image.open(img_path).convert("RGBA")
fff = Image.new(logo.mode, logo.size, (255,) * 4)

background = Image.new("RGBA", device.size, "white")
posn = ((device.width - logo.width) // 2, 0)

while True:
    for angle in range(0, 360, 2):
        rot = logo.rotate(angle, resample=Image.BILINEAR)
        img = Image.composite(rot, fff, rot)
        background.paste(img, posn)
        device.display(background.convert(device.mode))
