import bme280
import smbus2
from time import sleep
from influxdb import InfluxDBClient

# MBE280 connection
port = 1
address = 0x76
bus = smbus2.SMBus(port)

bme280.load_calibration_params(bus, address)


# InfluxDB connection
client = InfluxDBClient('localhost', 8086, 'grafana', 'grafana', 'skills')
client.switch_database('skills')


data = bme280.sample(bus, address)

temperature = round(data.temperature, 2)
humidity = round(data.humidity, 2)
pressure = round(data.pressure, 2)

line = f'bme280 temp={temperature},humidity={humidity},pressure={pressure}'
client.write([line], {'db': 'skills'}, 204, 'line')