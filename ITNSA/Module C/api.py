from flask import Flask
from flask_basicauth import BasicAuth

from influxdb import InfluxDBClient

# InfluxDB connection
client = InfluxDBClient('localhost', 8086, 'grafana', 'grafana', 'skills')
client.switch_database('skills')


app = Flask(__name__)


app.config['BASIC_AUTH_USERNAME'] = 'skills'
app.config['BASIC_AUTH_PASSWORD'] = 'euro'
basic_auth = BasicAuth(app)


@app.route('/data')
@basic_auth.required
def data():

    data = client.query(
        f'SELECT "temperature", "humidity" FROM "test" WHERE time >= now() - 30m;')
    return {
        'data': list(data.get_points(measurement='test')),
    }
