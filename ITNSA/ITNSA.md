# Zoom meeting

## General

- 18. 4. 2021 testiranje
- posnetke lahko pošljemo na wsuk@gassproductions.co.uk

## Module A

- 24.4.2021
- Networking, intergracija med Windows in Linux
- Azure Lab service
- AD, DNS na Windows
- HyperV
- Web, email, OpenVPN Linux (Apache, dovecot, ...)
- za firewall nftables namesto iptables ?
- load balancing ?
- OS že naloženi
- auto marking s skriptami
- VMji imajo drugačna imena kot na diagramu?

## Module B

- 25.4.2021
- Networking
- GNS3
- IPv6 only?
- veliko BGPja
- Azure Lab Service
- auto marking s skriptami

## [Module C](EuroSkills/ITNSA/Module%20C.md)

- 1.5.2021
- Raspberry Pi
- Raspberry OS Lite (2021-01-11-raspios-buster-armhf-lite.zip)
- local repo mirror za boljši performace
- IoT
- Python?
- Netboot, brez SD kartic
- 2 daisychainana displaya
- REST API
- reverse proxy
- senzorji na E2C, displayi na SPI?

## Module D

- 2.5.2021
- Troubleshooting
- secret tasks?
- native Azure environment
- 9 taskov

note: različni browserji za azure
