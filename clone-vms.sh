#!/bin/sh

# https://kb.vmware.com/s/article/1029513



#
# Uporaba: 
# chmod +x clone-vms.sh
# ./clone-vms.sh
# Še vedno je treba odgovoriti na vprašanje če si kopiral ali premaknil ("I copied it")
#

#
# Poglej registrirane VM:
# vim-cmd vmsvc/getallvms
#
# Unregister VM
# vim-cmd vmsvc/unregister VM_ID
# pobriši direktorije od VM v /vmfs/volumes/datastore1/
#

#
# Klonirani Windows VMi imajo identične SIDe
#

serverTemplate=SERVER
clientTemplate=CLIENT

# Servers
for i in 1 2 3 4 6 7 8 9 11 12
do
    vm_name=WINSRV$i

    mkdir /vmfs/volumes/datastore1/$vm_name

    cp -r /vmfs/volumes/datastore1/$serverTemplate/$serverTemplate.vmx /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx
    sed -i "s/$serverTemplate/$vm_name/g" /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx

    vmkfstools -i /vmfs/volumes/datastore1/$serverTemplate/$serverTemplate.vmdk /vmfs/volumes/datastore1/$vm_name/$vm_name.vmdk -d thin

    vim-cmd solo/registervm /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx
done


# Clients
for i in 1 2 3 4 5
do
    vm_name=WINCLI$i

    mkdir /vmfs/volumes/datastore1/$vm_name

    cp -r /vmfs/volumes/datastore1/$clientTemplate/$clientTemplate.vmx /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx
    sed -i "s/$clientTemplate/$vm_name/g" /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx

    vmkfstools -i /vmfs/volumes/datastore1/$clientTemplate/$clientTemplate.vmdk /vmfs/volumes/datastore1/$vm_name/$vm_name.vmdk -d thin

    vim-cmd solo/registervm /vmfs/volumes/datastore1/$vm_name/$vm_name.vmx
done
